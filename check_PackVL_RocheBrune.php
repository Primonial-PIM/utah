<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);


require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);
require_once 'includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconcilliation_dashboard.php');

//The following list must be updated when a new fund is created for the company
$rocheBruneFunds = "1310501;1310502;1310503;1310504;1310505";

if (NEOCAPTURE_DEBUG_ECHO) echo 'check_PackVL_RocheBrune.php, Start' . PHP_EOL;

ini_set('display_errors', 1);


$pendingDirectoryName = VLFILES;
$processedDirectoryName = VLFILES_PROCESSED;
$filePattern = VLFILES_PATTERN;
$directoryFiles = false;

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;


try
{
    $directoryFiles = scandir($pendingDirectoryName);
}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
}

try
{
    $sql_DateEntered = get_DateNow_sqlsvr();
    $capture = array();
    $capture['setID'] = 18;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $id = add_capture($capture);
    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if ($directoryFiles !== false)
    {
        foreach ($directoryFiles as $thisFile)
        {
            // is this file is a directory, especially . and .


            if (is_dir($pendingDirectoryName . '/' . $thisFile))
            {
                continue;
            }

            if (preg_match($filePattern, $thisFile))
            {
                echo 'Filename : ' . $thisFile . PHP_EOL;
                if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

                $objPHPExcel = PHPExcel_IOFactory::load($pendingDirectoryName . '/' . $thisFile);

                if ($objPHPExcel === false)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to load file ' . $thisFile) . PHP_EOL;

                    $DoMoveFile = false;
                }
                else
                {
                    $sheet_nbr = $objPHPExcel->getSheetCount();
                    //echo $sheet_nbr . " sheets in the xls file." . PHP_EOL;
                    if ($sheet_nbr < 12)
                    {
                        echo $sheet_nbr . " wrong sheets number in the xls file." . PHP_EOL;

                    }
                    else
                    {


                    //$sheets = $objPHPExcel->getSheetNames();
                    //print_r ($sheets)  . PHP_EOL;
                    $sheetDashboard = $objPHPExcel->getSheet(2); //There are 2 hidden sheets at 0 and 1 positions
                    $fundNameBrut = $sheetDashboard->getCell('A3')->getValue();
                    //$nameArray = explode($nameSeparator, $fundNameBrut);
                    //$fundCode = trim($nameArray[0]);
                    //$fundName = trim($nameArray[1]);
                    //$fundNameBrut = $sheetDashboard->getCell('A3')->getValue();
                    $fundCode = preg_replace("#.*(\(.*\)).*#s", "$1", $fundNameBrut);
                    $fundCode = str_replace(array( "(", ")"), "", $fundCode);


                    $isinCode = $sheetDashboard->getCell('A10')->getValue();
                    $fundNavDate = $sheetDashboard->getCell('B4')->getValue();
                    $fundNavDate = PHPExcel_Shared_Date::ExcelToPHP($fundNavDate);
                    $fundNavDate = date('d/m/Y', $fundNavDate);
                    $currency = $sheetDashboard->getCell('B6')->getValue();

                    $lastRow = $sheetDashboard->getHighestRow();
                    echo '---------------------------------' . PHP_EOL;
                    echo 'FundNameBrut : ' . $fundNameBrut . PHP_EOL;
                    echo 'FundCode : ' . $fundCode . PHP_EOL;
                    echo 'isinCode : ' . $isinCode . PHP_EOL;
                    echo 'FundNavDate : ' . $fundNavDate . PHP_EOL;
                    //echo 'FundName : ' . $fundName . PHP_EOL;
                    //echo 'Debut FundName : ' . $debut . PHP_EOL;
                    //echo 'ISIN : ' . $isinCode . PHP_EOL;
                    //echo 'FundNavDate : ' . $fundNavDate . PHP_EOL;
                    //echo 'Currency : ' . $currency . PHP_EOL;
                    //echo 'Lines Nbr : ' . $lastRow . PHP_EOL;
                    }

                    $params = array();

                    //if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has lines.', count($lastRow)) . PHP_EOL;

                    $fundPattern = '/'. $fundCode . '/';

                    //if (strpos($rocheBruneFunds,$fundCode) >= 0)
                    if ($fundCode and preg_match($fundPattern,$rocheBruneFunds) and $sheet_nbr > 12)
                    {

                        echo 'RocheBrune PackVL valid file --- processing ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;
                        /* Available sheets
                        Array
                        (
                        [0] => _Hidden1
                        [1] => _Hidden2
                        [2] => Signalétique
                        [3] => Synthèse de valorisation
                        [4] => Portefeuille
                        [5] => Disponibilités
                        [6] => Taux de change
                        [7] => Autres Impacts
                        [8] => _Hidden9
                        [9] => _Hidden10
                        [10] => _Hidden11
                        [11] => Répartition Portefeuille
                        [12] => Frais de gestion
                        [13] => Echéancier
                        [14] => Mouvements
                        [15] => Synthèse monétaire
                        ) */

                        // Initialise all interesting sheets
                        $sheetDashboard = $objPHPExcel->getSheet(0);
                        $sheetValuation = $objPHPExcel->getSheet(2);
                        $sheetInventory = $objPHPExcel->getSheet(4);
                        $sheetAvailabilities = $objPHPExcel->getSheet(5);
                        $sheetExchangeRate = $objPHPExcel->getSheet(6);
                        $sheetOtherImpacts = $objPHPExcel->getSheet(7);
                        $sheetMovements = $objPHPExcel->getSheet(14);
                        $sheetFeesList = $objPHPExcel->getSheet(12);
                        $sheetFeesAmount = $objPHPExcel->getSheet(12);

                        // ExchangeRate
                        $currentSheet = $sheetExchangeRate;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Exchange Rate '. PHP_EOL;
                        xls_exchangerate_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);


                        // Valuation
                        $currentSheet = $sheetValuation;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Valuation ' . PHP_EOL;
                        xls_valuation_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);

                        // Availabilities
                        $currentSheet = $sheetAvailabilities;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees List '. PHP_EOL;
                        xls_availabilities_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);

                        // Movements
                        $currentSheet = $sheetMovements;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees List '. PHP_EOL;
                        xls_movements_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);

                        // Fees list
                        $currentSheet = $sheetFeesList;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees List '. PHP_EOL;
                        xls_feeslist_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);

                        //Fees amount
                        $currentSheet = $sheetFeesAmount;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees List '. PHP_EOL;
                        //xls_feesamount_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode);


                        // Save events file entries.
                        $DoMoveFile = true;



                    } // If Count($lines)
                    else
                    {
                        echo 'not interesting or not for RocheBrune ---- skip the file ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';

                        $DoMoveFile = false;

                    }


                }

                if ($DoMoveFile)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

                    try
                    {
                        $nowDate = New DateTime();
                    }
                    catch (Exception $e)
                    {
                        // In the case of an exception, probable because the timezone is not set in php.ini, default to Paris.
                        date_default_timezone_set('Europe/Paris');
                        $nowDate = New DateTime();
                    }

                    $newFile = $nowDate->format('YmdHis') . '_' . $thisFile;

                    $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
                    if ($copyResult)
                    {
                        unlink(($pendingDirectoryName . '/' . $thisFile));
                    }
                    else
                    {
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
                    }

                }
                else
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
                }

            } // If preg_match()
            else
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
            }

        } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
    else
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
    capturefailed('check_PackVL_RocheBrune : ' . $e->getMessage(), $thisFile);
}


function xls_feeslist_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `feeslist sheet`' . PHP_EOL;



    try
    {

        $rowNbr = 1;
        foreach($currentSheet->getRowIterator() as $row) {

            // in this sheet the currency are listed after the 9th line
            if ($rowNbr > 9)
            {

                /*
                ALTER PROCEDURE [dbo].[adp_rec_Fees_InsertCommand]
                (
                @IDENTITY int OUTPUT,
                @captureID int,
                @NAVDate [nvarchar](50),
                @FundCode [nvarchar](50),
                @neo_class [nvarchar](50),
                @neo_feetype [nvarchar](50),
                @neo_fixedamount [nvarchar](50),
                @neo_minthreshold [nvarchar](50),
                @neo_maxthreshold [nvarchar](50),
                @neo_rate [nvarchar](50),
                @neo_currency [nvarchar](5),
                @neo_DateEntered nvarchar(50)
                )
                AS

                * */
                $feesLine = array();
                $feesLine['captureID'] = $sqlsvr_id;
                $feesLine['fundCode'] = $fundCode;
                $feesLine['NAVDate'] = $fundNavDate;
                $feesLine['class'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $feesLine['feetype'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $feesLine['fixedamount'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $feesLine['minthreshold'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();
                $feesLine['maxthreshold'] = $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $feesLine['rate'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $feesLine['currency'] = '';

                if ($feesLine['class'] == 'REPARTITION DES FRAIS')
                {

                    break;
                }

                if ($feesLine['class'])
                {

                    insert_feeslist_sqlsrv($feesLine);

                }


            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}


function xls_feesamount_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `feesamount sheet`' . PHP_EOL;



    try
    {

        $rowNbr = 1;
        foreach($currentSheet->getRowIterator() as $row) {


            if  ($rowNbr > 12)
            {
                $feesAmountLine = array();
                $feesAmountLine['captureID'] = $sqlsvr_id;
                $feesAmountLine['fundCode'] = $fundCode;
                $feesAmountLine['NAVDate'] = $fundNavDate;
                $feesAmountLine['class'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $feesAmountLine['feetype'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $feesAmountLine['feesamount'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $feesAmountLine['feescumulative'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                //echo 'feesamount : ' . $feesAmountLine['feesamount'] . PHP_EOL;
                //echo 'feesamountcumu : ' . $feesAmountLine['feescumulative'] . PHP_EOL;

                if ($feesAmountLine['class'])
                {
                    insert_feesamount_sqlsrv($feesAmountLine);
                }

            }


            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}


function xls_exchangerate_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `exchange rate sheet`' . PHP_EOL;



    try
    {

        $rowNbr = 1;
        foreach($currentSheet->getRowIterator() as $row) {

            // in this sheet the currency are listed after the 9th line
            if ($rowNbr > 9)
            {
                $rateLine = array();
                $rateLine['captureID'] = $sqlsvr_id;
                $rateLine['fundCode'] = $fundCode;
                $rateLine['NAVDate'] = $fundNavDate;
                $rateLine['currency'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $rateLine['ratedate'] = $currentSheet->getCell('B' . (string)$rowNbr)->getValue();
                $rateLine['rate'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $rateLine['previousratedate'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $rateLine['previousrate'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $rateLine['percentchange'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();

                $dt = PHPExcel_Shared_Date::ExcelToPHP($rateLine['ratedate']);
                $dt = date('d/m/Y', $dt);
                $rateLine['ratedate'] = $dt;

                $pdt = PHPExcel_Shared_Date::ExcelToPHP($rateLine['previousratedate']);
                $pdt = date('d/m/Y', $pdt);
                $rateLine['previousratedate'] = $pdt;

                if ($rateLine['currency'])
                {
                    //echo 'CCY  : ' . $rateLine['currency'] . PHP_EOL;
                    //echo 'NAVDATE   : ' . $rateLine['NAVDate'] . PHP_EOL;
                    //echo 'DATE   : ' . $dt . PHP_EOL;
                    //echo 'RATE : ' . $rateLine['rate'] . PHP_EOL;
                    //echo 'PREVDATE : ' . $pdt . PHP_EOL;
                    //echo 'PREVRATE : ' . $rateLine['previousrate'] . PHP_EOL;
                    //echo 'VARIATION : ' . $rateLine['percentchange'] . PHP_EOL;
                    insert_exchangerate_sqlsrv($rateLine);

                }
            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}


function insert_exchangerate_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

// Create a new stored prodecure
        $stmt = mssql_init('adp_rec_FX_InsertCommand', $sqlsvr_conn);

// Bind the field names

        /*
         * PROCEDURE [dbo].[adp_rec_FX_InsertCommand]
              (
              @IDENTITY int OUTPUT,
              @captureID int,
              @NAVDate [nvarchar](50),
              @FundCode [nvarchar](50),
              @neo_currency [nvarchar](5),
              @neo_ratedate [nvarchar](50),
              @neo_rate [nvarchar](50),
              @neo_previousratedate [nvarchar](50),
              @neo_previousrate [nvarchar](50),
              @neo_percentchange [nvarchar](50),
              @neo_DateEntered nvarchar(50)
              )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_currency', &$params['currency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_ratedate', &$params['ratedate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_rate', &$params['rate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_previousratedate', &$params['previousratedate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_previousrate',  &$params['previousrate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_percentchange', &$params['percentchange'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}


function xls_valuation_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `valuation sheet`' . PHP_EOL;


    try
    {

        $rowNbr = 1;


        foreach($currentSheet->getRowIterator() as $row) {
            $valuationLine = array();
            // in this sheet the currency are listed after the 9th line
            if ($rowNbr > 9)
            {

                // A3 content example : 1310032 - PRIMONIAL OR

                $valuationLine['captureID'] = $sqlsvr_id;
                $valuationLine['fundCode'] = $fundCode;
                $valuationLine['NAVDate'] = $fundNavDate;
                $valuationLine['isin'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $valuationLine['sharetype'] = $currentSheet->getCell('B' . (string)$rowNbr)->getValue();
                $valuationLine['currency'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $valuationLine['nav'] = $currentSheet->getCell('E' . (string)$rowNbr)->getValue();
                $valuationLine['prevnav'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $valuationLine['navchange'] = $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $valuationLine['navchange'] = $valuationLine['navchange'] . '%';
                $valuationLine['netassets'] = $currentSheet->getCell('L' . (string)$rowNbr)->getValue();
                $valuationLine['numbershares'] = $currentSheet->getCell('K' . (string)$rowNbr)->getValue();
                $valuationLine['soubscriptions'] = ''; // Doesn't exist in the current VL file
                $valuationLine['redemptions'] = ''; // Doesn't exist in the current VL file




                if ($valuationLine['isin'])
                {
                    //echo 'FUND CODE  : ' . $valuationLine['fundCode'] . PHP_EOL;
                    //echo 'ISIN  : ' . $valuationLine['isin'] . PHP_EOL;
                    //echo 'NAVDATE   : ' . $valuationLine['NAVDate'] . PHP_EOL;
                    //echo 'CCY  : ' . $valuationLine['currency'] . PHP_EOL;
                    //echo 'NAV   : ' . $valuationLine['nav'] . PHP_EOL;
                    //echo 'PRENAV  : ' . $valuationLine['prevnav'] . PHP_EOL;
                    //echo 'NAVCHANGE  : ' . $valuationLine['navchange'] . PHP_EOL;
                    //echo 'NETASSETS : ' . $valuationLine['netassets'] . PHP_EOL;
                    //echo 'NBSHARES : ' . $valuationLine['numbershares'] . PHP_EOL;

                    insert_valuation_sqlsrv($valuationLine);
                }else
                {
                    break;
                }

            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}

function insert_valuation_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_Valuation_InsertCommand', $sqlsvr_conn);



        /*
         * PROCEDURE [dbo].adp_rec_Valuation_InsertCommand]
            (
            @IDENTITY int OUTPUT,
            @captureID int,
            @NAVDate [nvarchar](50),
            @FundCode [nvarchar](50),
            @neo_isin [nvarchar](50),
            @neo_sharetype [nvarchar](50),
            @neo_currency [nvarchar](5),
            @neo_nav [nvarchar](50),
            @neo_navchange [nvarchar](50),
            @neo_netassets [nvarchar](50),
            @neo_numbershares [nvarchar](50),
            @neo_subscriptions [nvarchar](50),
            @neo_redemptions [nvarchar](50),
            @neo_DateEntered nvarchar(50)
            )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_isin', &$params['isin'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_sharetype', &$params['sharetype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_currency', &$params['currency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_nav', &$params['nav'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_navchange', &$params['navchange'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_netassets', &$params['netassets'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_numbershares',  &$params['numbershares'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_subscriptions',  &$params['subscriptions'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_redemptions', &$params['redemptions'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}

function xls_availabilities_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `Availabilities sheet`' . PHP_EOL;



    try
    {

        $rowNbr = 1;
        foreach($currentSheet->getRowIterator() as $row) {


            if ($rowNbr > 9)
            {

                $availabilityLine = array();
                $availabilityLine['captureID'] = $sqlsvr_id;
                $availabilityLine['fundCode'] = $fundCode;
                $availabilityLine['NAVDate'] = $fundNavDate;
                $availabilityLine['description'] = $currentSheet->getCell('B' . (string)$rowNbr)->getValue();
                $availabilityLine['percentassets'] = $currentSheet->getCell('E' . (string)$rowNbr)->getValue();
                $availabilityLine['percentavailabilities'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $availabilityLine['currency'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();
                $availabilityLine['balance'] = $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $availabilityLine['currentbalance'] = $currentSheet->getCell('I' . (string)$rowNbr)->getValue();
                $availabilityLine['previousbalance'] = $currentSheet->getCell('J' . (string)$rowNbr)->getValue();
                $availabilityLine['previouscurrentbalance'] = $currentSheet->getCell('K' . (string)$rowNbr)->getValue();
                $availabilityLine['balancevariation'] = $currentSheet->getCell('L' . (string)$rowNbr)->getValue();
                $availabilityLine['percentvariation'] = $currentSheet->getCell('M' . (string)$rowNbr)->getValue();
                $availabilityLine['percentvariation'] = $availabilityLine['percentvariation'] . '%';

                if ($availabilityLine['description'])

                {

                    echo '*******************************************'  . PHP_EOL;
                    echo 'DESCRIPT   : ' . $availabilityLine['description'] . PHP_EOL;
                    echo 'PERCENTASSETS : ' .  $availabilityLine['percentassets'] . PHP_EOL;
                    echo 'PERCENTAVAIL : ' . $availabilityLine['percentavailabilities'] . PHP_EOL;
                    echo 'CCY  : ' . $availabilityLine['currency'] . PHP_EOL;
                    echo 'BALA : ' . $availabilityLine['balance'] . PHP_EOL;
                    echo 'CURRENT BAL : ' . $availabilityLine['currentbalance'] . PHP_EOL;
                    echo 'PREV  BAL : ' . $availabilityLine['previousbalance'] . PHP_EOL;
                    echo 'PREV  CUR BAL : ' . $availabilityLine['previouscurrentbalance'] . PHP_EOL;
                    echo 'BAL VARIATION : ' . $availabilityLine['balancevariation'] . PHP_EOL;
                    echo 'PCT VARIATION : ' . $availabilityLine['percentvariation'] . PHP_EOL;
                    echo '*******************************************'  . PHP_EOL;
                    insert_availabilities_sqlsrv($availabilityLine);

                }
            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}


function insert_availabilities_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        ini_set('display_errors', 1);
        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_Availabilitites_InsertCommand', $sqlsvr_conn);



        /*
         PROCEDURE [dbo].[adp_rec_Availabilitites_InsertCommand]
            (
            @IDENTITY int OUTPUT,
            @captureID int,
            @NAVDate [nvarchar](50),
            @FundCode [nvarchar](50),
            @neo_description [nvarchar](255),
            @neo_percentassets [nvarchar](50),
            @neo_percentavailabilities [nvarchar](50),
            @neo_currency [nvarchar](5),
            @neo_balance [nvarchar](50),
            @neo_currentbalance [nvarchar](50),
            @neo_previousbalance [nvarchar](50),
            @neo_previouscurrentbalance [nvarchar](50),
            @neo_balancevariation [nvarchar](50),
            @neo_percentvariation [nvarchar](50),
            @neo_DateEntered nvarchar(50)
            )
          * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_description', &$params['description'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_percentassets', &$params['percentassets'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_percentavailabilities', &$params['percentavailabilities'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_currency', &$params['currency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_balance', &$params['balance'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_currentbalance', &$params['currentbalance'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_previousbalance', &$params['previousbalance'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_previouscurrentbalance',  &$params['previouscurrentbalance'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_balancevariation',  &$params['balancevariation'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_percentvariation', &$params['percentvariation'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}


function xls_movements_table($currentSheet, $sqlsvr_id, $fundNavDate, $fundCode)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `Movements sheet`' . PHP_EOL;

    //$movementLine = array();

    try
    {

        $rowNbr = 1;
        $lastRow = $currentSheet->getHighestRow();
        $linecount = $lastRow - 10; //there is an empty line at the end of the shift
        foreach($currentSheet->getRowIterator() as $row) {


            if ($rowNbr > 9)
            {
                $movementLine = array();
                /*
                @captureID int,
                @neo_clickall nvarchar(50),
                @neo_clientreference nvarchar(50), V
                @neo_movementtype nvarchar(50),  C
                @neo_cancellation nvarchar(50),  A
                @neo_securitycode nvarchar(50), D
                @neo_quantity nvarchar(50), H
                @neo_quantityflag nvarchar(50), AH
                @neo_price nvarchar(50), S
                @neo_priceflag nvarchar(50), AI
                @neo_quotecurrency nvarchar(5), R
                @neo_netamount_pc nvarchar(50), AO
                @neo_paymentcurrency nvarchar(5), AE
                @neo_fees_rc nvarchar(50), K
                @neo_costprice_rc nvarchar(50), AD
                @neo_unrealisedgainloss_fundcurrency nvarchar(50), T
                @neo_referencecurrency nvarchar(5), AK
                @fundcode nvarchar(50), in the call
                @neo_tradedate nvarchar(50), G
                @neo_valuedate nvarchar(50), AA
                @neo_managementdate nvarchar(50), Z
                @neo_newquantity nvarchar(50), L
                @neo_linecount nvarchar(50), rowcount - 9
                @neo_DateEntered nvarchar(50)
                */



                $movementLine['captureID'] = $sqlsvr_id;
                $movementLine['fundcode'] = $fundCode;
                $movementLine['NAVDate'] = $fundNavDate;
                $movementLine['clickall'] = '';
                $movementLine['clientreference'] = $currentSheet->getCell('V' . (string)$rowNbr)->getValue();
                $movementLine['movementtype'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $movementLine ['cancellation'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $movementLine ['securitycode'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $movementLine ['quantity'] = $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $movementLine ['quantityflag'] = $currentSheet->getCell('AH' . (string)$rowNbr)->getValue();
                $movementLine ['price'] = $currentSheet->getCell('S' . (string)$rowNbr)->getValue();
                $movementLine ['priceflag'] = $currentSheet->getCell('AI' . (string)$rowNbr)->getValue();
                $movementLine ['quotecurrency'] = $currentSheet->getCell('R' . (string)$rowNbr)->getValue();
                $movementLine ['netamount_pc'] = $currentSheet->getCell('AO' . (string)$rowNbr)->getValue();
                $movementLine ['paymentcurrency'] = $currentSheet->getCell('AE' . (string)$rowNbr)->getValue();
                $movementLine ['fees_rc'] = $currentSheet->getCell('K' . (string)$rowNbr)->getValue();
                $movementLine ['costprice_rc'] = $currentSheet->getCell('AD' . (string)$rowNbr)->getValue();
                $movementLine ['unrealisedgainloss_fundcurrency'] = $currentSheet->getCell('T' . (string)$rowNbr)->getValue();
                $movementLine ['referencecurrency'] = $currentSheet->getCell('AK' . (string)$rowNbr)->getValue();
                $movementLine ['tradedate'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();
                $movementLine ['valuedate'] = $currentSheet->getCell('AA' . (string)$rowNbr)->getValue();
                $movementLine ['managementdate'] = $currentSheet->getCell('Z' . (string)$rowNbr)->getValue();
                $movementLine ['newquantity'] = $currentSheet->getCell('L' . (string)$rowNbr)->getValue();
                $movementLine ['linecount'] = (string)$linecount;

                // Change date format
                $mdt = PHPExcel_Shared_Date::ExcelToPHP($movementLine['managementdate']);
                $mdt = date('d/m/Y', $mdt);
                $movementLine['managementdate'] = $mdt;

                $vdt = PHPExcel_Shared_Date::ExcelToPHP($movementLine['valuedate']);
                $vdt = date('d/m/Y', $vdt);
                $movementLine['valuedate'] = $vdt;

                $tdt = PHPExcel_Shared_Date::ExcelToPHP($movementLine['tradedate']);
                $tdt = date('d/m/Y', $tdt);
                $movementLine['tradedate'] = $tdt;


                if ($movementLine['movementtype'])
                {

                    echo '*******************************************'  . PHP_EOL;
                    echo 'FUND CODE   : ' . $movementLine['fundcode'] . PHP_EOL;
                    echo 'clientreference : ' .  $movementLine['clientreference'] . PHP_EOL;
                    echo 'movementtype : ' . $movementLine['movementtype'] . PHP_EOL;
                    echo 'cancellation  : ' . $movementLine['cancellation'] . PHP_EOL;
                    echo 'securitycode : ' . $movementLine['securitycode'] . PHP_EOL;
                    echo 'linecount : ' . $movementLine['linecount'] . PHP_EOL;
                    insert_movements_sqlsrv($movementLine);
                }
            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}

function insert_movements_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_movement_InsertCommand', $sqlsvr_conn);

        /*
        ALTER PROCEDURE [dbo].[adp_rec_movement_InsertCommand]
            (
            @IDENTITY int OUTPUT,
            @captureID int,
            @neo_clickall nvarchar(50),
            @neo_clientreference nvarchar(50),
            @neo_movementtype nvarchar(50),  H
            @neo_cancellation nvarchar(50),  AS
            @neo_securitycode nvarchar(50), F
            @neo_quantity nvarchar(50), J
            @neo_quantityflag nvarchar(50), E
            @neo_price nvarchar(50), AA
            @neo_priceflag nvarchar(50),
            @neo_quotecurrency nvarchar(5),
            @neo_netamount_pc nvarchar(50),
            @neo_paymentcurrency nvarchar(5),
            @neo_fees_rc nvarchar(50),
            @neo_costprice_rc nvarchar(50),
            @neo_unrealisedgainloss_fundcurrency nvarchar(50),
            @neo_referencecurrency nvarchar(5),
            @fundcode nvarchar(50),
            @neo_tradedate nvarchar(50),
            @neo_valuedate nvarchar(50),
            @neo_managementdate nvarchar(50),
            @neo_newquantity nvarchar(50),
            @neo_linecount nvarchar(50),
            @neo_DateEntered nvarchar(50)
            )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@neo_clickall', &$params['clickall'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_clientreference',&$params['clientreference'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_movementtype', &$params['movementtype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_cancellation', &$params['cancellation'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_securitycode', &$params['securitycode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quantity', &$params['quantity'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quantityflag', &$params['quantityflag'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_price', &$params['price'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_priceflag', &$params['priceflag'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quotecurrency',&$params['quotecurrency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_netamount_pc', &$params['netamount_pc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_paymentcurrency', &$params['paymentcurrency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_fees_rc', &$params['fees_rc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_costprice_rc', &$params['costprice_rc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_unrealisedgainloss_fundcurrency', &$params['unrealisedgainloss_fundcurrency'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_referencecurrency', &$params['referencecurrency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@fundcode', &$params['fundcode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_tradedate', &$params['tradedate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_valuedate', &$params['valuedate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_managementdate', &$params['managementdate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_newquantity', &$params['newquantity'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_linecount', &$params['linecount'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}


function insert_feeslist_sqlsrv($params)
{
    try
    {
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_Fees_InsertCommand', $sqlsvr_conn);

        // Bind the field names

        /*
         * PROCEDURE [dbo].[adp_rec_Fees_InsertCommand]
              (
              @IDENTITY int OUTPUT,
              @captureID int,
              @NAVDate [nvarchar](50),
              @FundCode [nvarchar](50),
              @neo_class [nvarchar](50),
              @neo_feetype [nvarchar](50),
              @neo_fixedamount [nvarchar](50),
              @neo_minthreshold [nvarchar](50),
              @neo_maxthreshold [nvarchar](50),
              @neo_rate [nvarchar](50),
              @neo_currency [nvarchar](5),
              @neo_DateEntered nvarchar(50)
              )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_class', &$params['class'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feetype', &$params['feetype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_fixedamount', &$params['fixedamount'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_minthreshold', &$params['minthreshold'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_maxthreshold', &$params['maxthreshold'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_rate', &$params['rate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_currency', &$params['currency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}


function insert_feesamount_sqlsrv($params)
{
    try
    {
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_FeesAmount_InsertCommand', $sqlsvr_conn);

        // Bind the field names

        /*
              (
              PROCEDURE [dbo].[adp_rec_FeesAmount_InsertCommand]
                    (
                    @IDENTITY int OUTPUT,
                    @captureID int,
                    @NAVDate [nvarchar](50),
                    @FundCode [nvarchar](50),
                    @neo_class [nvarchar](50),
                    @neo_feetype [nvarchar](50),
                    @neo_feesamount [nvarchar](50),
                    @neo_feescumulative[nvarchar](50),
                    @neo_DateEntered nvarchar(50)
                    )
              )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_class', &$params['class'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feetype', &$params['feetype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feesamount', &$params['feesamount'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feescumulative', &$params['feescumulative'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}


function capturefailed($message)
{
    $capture = array();
    $capture['setID'] = 18;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = $message;
    $sql_DateEntered = get_DateNow_sqlsvr();
    add_capture_sqlserver($capture, $sql_DateEntered);
}
?>
