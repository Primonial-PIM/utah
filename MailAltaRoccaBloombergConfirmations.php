<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once('functions/db_functions.php');
require_once('data/data_capture.php');
//require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'MailAltaRoccaBloombergConfirmations.php, Start' . PHP_EOL;

//ini_set('display_errors', 1);

//require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
//require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');


// Get trade confirmations from pending directory

$pendingDirectoryName = '/mnt/bloombergFtpFiles';
$processedDirectoryName = '/mnt/bloombergFtpFiles/processed';
$filePattern = '/BlockFTP/';
$fileSeparator = ',';

//$pendingDirectoryName = '.';
//$processedDirectoryName = '.';
//$filePattern = '/BlockFTP/';
//$fileSeparator = ',';

$orderLine = array();

// intl Formatters.

$Formatter_Decimal = numfmt_create('fr_FR', NumberFormatter::DECIMAL );
$Formatter_Date = datefmt_create ('fr_FR', IntlDateFormatter::SHORT , IntlDateFormatter::NONE);

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;

try
  {
  $directoryFiles = scandir($pendingDirectoryName);
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if (strlen(date_default_timezone_get()) == 0)
    {
    date_default_timezone_set('UTC');
    }
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if ($directoryFiles !== false)
    {
    foreach ($directoryFiles as $thisFile)
      {
      // is this file is a directory, especially . and ..

      if (is_dir($pendingDirectoryName . '/' . $thisFile))
        {
        continue;
        }

      if (preg_match($filePattern, $thisFile))
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

        /* Log this file */
        
        /*
        $capture = array();
        $capture['setID'] = 9;
        $capture['result'] = 'Subscriptions, Processing ' . $pendingDirectoryName . '/' . $thisFile;
        $capture['filename'] = $thisFile;
        $capture['priority'] = 0;
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
        */

        /* Process this file*/
        $filecontents = file_get_contents($pendingDirectoryName . '/' . $thisFile);

        if ($filecontents === false)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to read file ' . $thisFile) . PHP_EOL;

          $DoMoveFile = false;
          }
        else
          {
          $lines = explode("\n", $filecontents);
          $orders_array = array();

          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;
          $idx = 1;
          if (count($lines) > 0)
            {

              foreach ($lines as $line)
              {

                    $orderFields = explode($fileSeparator, $line);
                    $orderLine = array();

                    //Skip the 1st line - columns name - and process only if line is not empty and
                    if ($idx > 1 and $orderFields[0])
                    {

                        $orderLine['BloombergFileReceived'] = $thisFile;
                        $orderLine['Text'] = $line;
                        $orderLine['Status'] = trim($orderFields[0]);
                        $orderLine['Side'] = trim($orderFields[1]);
                        $orderLine['Security'] = trim($orderFields[2]);
                        $orderLine['Isin'] = trim($orderFields[3]);
                        $orderLine['Broker'] = trim($orderFields[4]);
                        $orderLine['BrokerName'] = trim($orderFields[5]);
                        $orderLine['Price'] = trim($orderFields[6]);
                        $orderLine['Quantity'] = trim($orderFields[7]);
                        $orderLine['Principal'] = trim($orderFields[8]);
                        $orderLine['AccruedInterest'] = trim($orderFields[9]);
                        $orderLine['CleanPrice'] = trim($orderFields[10]);
                        $orderLine['DirtyPrice'] = trim($orderFields[11]);
                        $orderLine['Currency'] = trim($orderFields[12]);
                        $orderLine['TradeDate'] = trim($orderFields[13]);
                        $orderLine['TradeTime'] = trim($orderFields[14]);
                        $orderLine['ExecTime'] = trim($orderFields[15]);
                        $orderLine['SettlementDate'] = trim($orderFields[16]);
                        $orderLine['UserName'] = trim($orderFields[17]);
                        $orderLine['Sequence#'] = trim($orderFields[18]);
                        $orderLine['AllAccId'] = trim($orderFields[19]);
                        $orderLine['AllAccNum'] = trim($orderFields[20]);
                        $orderLine['AllId'] = trim($orderFields[21]);
                        $orderLine['AllocPrice'] = trim($orderFields[22]);
                        $orderLine['CoverBkr'] = trim($orderFields[23]);
                        $orderLine['Cover'] = trim($orderFields[24]);
                        $orderLine['CoverBkr2'] = trim($orderFields[25]);
                        $orderLine['Cover2'] = trim($orderFields[26]);
                        $orderLine['CoverBkr3'] = trim($orderFields[27]);
                        $orderLine['Cover3'] = trim($orderFields[28]);
                        $orderLine['CoverBkr4'] = trim($orderFields[29]);
                        $orderLine['Cover4'] = trim($orderFields[30]);
                        $orderLine['CoverBkr5'] = trim($orderFields[31]);
                        $orderLine['Cover5'] = trim($orderFields[32]);

                        $orders_array[] = $orderLine;
                    }

                    $idx++;
              }

            } // If Count($lines)
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
            }

          ///$DoMoveFile = true;
          $DoMoveFile = false;

          if (count($orders_array) > 0)
          {

              $details = print_r($orders_array,true);
              echo $details;

              //ini_set ("SMTP","mail.primonial.fr");
              $subject = "AltaRocca Execution Confirmation Received From Bloomberg";
              $message = $details;
              $headers = "From: ITSUPPORTAM@primonial.fr";
              $mailto = "mo_pam@primonial.fr, rodolphe.ruzindana@primonial.fr, martin.greggory@altarocca-am.fr";

              //$message = wordwrap($message, 70, "\r\n");

              // SendMail
              mail($mailto, $subject, $message, $headers);

              // Save the line in neocapture database - there is currently 1 line per vcon file
              // If Primonial change their mind and want 1 vcon file per day a loop will need to be done here
              insert_vcon_sqlsrv($orders_array[0]);

              $DoMoveFile = true;

           }
          }


        // Move Subscriptions / Redemptions file to Processed directory.

        if ($DoMoveFile)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

          try
            {
            $nowDate = New DateTime();
            }
          catch (Exception $e)
            {
            // In the case of an exception, probably because the timezone is not set in php.ini, default to Paris.
            date_default_timezone_set('Europe/Paris');
            $nowDate = New DateTime();
            }

          $newFile = $nowDate->format('YmdHis') . '_' . $thisFile;


          //$copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
          $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $thisFile));
          if ($copyResult)
            {
            $capture = array();
            $capture['setID'] = 23;
            $capture['result'] = 'success';
            $capture['filename'] = $thisFile;
            $capture['priority'] = 0;
            $sql_DateEntered = get_DateNow_sqlsvr();
            $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

            unlink(($pendingDirectoryName . '/' . $thisFile));
            }
          else
            {
            	$capture = array();
            	$capture['setID'] = 23;
            	$capture['result'] = 'failed to copy';
            	$capture['filename'] = $thisFile;
            	$capture['priority'] = 0;
            	$sql_DateEntered = get_DateNow_sqlsvr();
            	$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
            }

          }
        else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
          }

        } // If preg_match()
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
        }

      } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
  else
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

  if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  capturefailed('MailAltaRoccaBloombergConfirmations, failed to process file : ' . $e->getMessage(), $thisFile);
  }

function insert_vcon_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);
        print_r($params);
        // Create a new stored prodecure
        $stmt = mssql_init('adp_bloombergVconFiles_InsertCommand', $sqlsvr_conn);

        /*
         *
            ALTER PROCEDURE [dbo].[adp_bloombergVconFiles_InsertCommand]
                (
                @IDENTITY int OUTPUT,
                @vconFileDate nvarchar(50),
                @vconFileName nvarchar(50),
                @vconLine nvarchar(50),
                @status nvarchar(50),
                @side nvarchar(50),
                @security nvarchar(50),
                @isin nvarchar(50),
                @broker nvarchar(50),
                @brokerName nvarchar(50),
                @price nvarchar(50),
                @quantity nvarchar(50),
                @principal nvarchar(50),
                @accruedInterest nvarchar(50),
                @cleanPrice nvarchar(50),
                @dirtyPrice nvarchar(50),
                @currency nvarchar(50),
                @tradeDate nvarchar(50),
                @tradeTime nvarchar(50),
                @settlementDate nvarchar(50),
                @userName nvarchar(50),
                @sequenceNumber nvarchar(50)

          )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@vconFileDate', &$params['TradeDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@vconFileName',&$params['BloombergFileReceived'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@vconLine', &$params['Text'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@status', &$params['Status'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@side', &$params['Side'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@security', &$params['Security'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@isin', &$params['Isin'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@broker', &$params['Broker'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@brokerName', &$params['BrokerName'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@price', &$params['Price'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@quantity', &$params['Quantity'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@principal', &$params['Principal'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@accruedInterest', &$params['AccruedInterest'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@cleanPrice', &$params['CleanPrice'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@dirtyPrice', &$params['DirtyPrice'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@currency', &$params['Currency'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@tradeDate', &$params['TradeDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@tradeTime', &$params['TradeTime'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@execTime', &$params['ExecTime'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@settlementDate', &$params['SettlementDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@userName', &$params['UserName'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@sequenceNumber', &$params['Sequence#'], SQLVARCHAR, false, false, 50);

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}




?>
