<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);
/**
 * Created by JetBrains PhpStorm.
 * User: nicholas
 * Date: 14/05/2013
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */

require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'checkSubscriptionsBNPParis.php, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');


// Get trade confirmations from pending directory

$pendingDirectoryName = PARIS_ORDERFILES;
$processedDirectoryName = PARIS_ORDERFILES_PROCESSED;
$filePattern = PARIS_ORDERFILES_PATTERN;
$fileSeparator = PARIS_ORDERFILES_SEPARATOR;

$orderLine = array();

// intl Formatters.

$Formatter_Decimal = numfmt_create( PARIS_ORDERFILES_LOCALE, NumberFormatter::DECIMAL );
$Formatter_Date = datefmt_create ( PARIS_ORDERFILES_LOCALE, IntlDateFormatter::SHORT , IntlDateFormatter::NONE);

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;

try
  {
  $directoryFiles = scandir($pendingDirectoryName);
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if (strlen(date_default_timezone_get()) == 0)
    {
    date_default_timezone_set('UTC');
    }
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if ($directoryFiles !== false)
    {
    foreach ($directoryFiles as $thisFile)
      {
      // is this file is a directory, especially . and ..

      if (is_dir($pendingDirectoryName . '/' . $thisFile))
        {
        continue;
        }

      if (preg_match($filePattern, $thisFile))
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

        /* Log this file */
        
        /*
        $capture = array();
        $capture['setID'] = 9;
        $capture['result'] = 'Subscriptions, Processing ' . $pendingDirectoryName . '/' . $thisFile;
        $capture['filename'] = $thisFile;
        $capture['priority'] = 0;
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
        */

        /* Process this file*/
        $filecontents = file_get_contents($pendingDirectoryName . '/' . $thisFile);

        if ($filecontents === false)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to read file ' . $thisFile) . PHP_EOL;

          $DoMoveFile = false;
          }
        else
          {
          $lines = explode("\n", $filecontents);
          $orders_array = array();

          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;

          if (count($lines) > 0)
            {

            foreach ($lines as $line)
              {
              $orderFields = explode($fileSeparator, $line);

              //

              if  (count($orderFields) >= 38)
                {
                $orderLine = array();

                // Check numeric fields are not Zero length

                $orderFields[29] = str_replace(' ','', $orderFields[29]);
                $orderFields[30] = str_replace(' ','', $orderFields[30]);
                $orderFields[31] = str_replace(' ','', $orderFields[31]);

                if (strlen($orderFields[29]) == 0) $orderFields[29] = "0";
                if (strlen($orderFields[30]) == 0) $orderFields[30] = "0";
                if (strlen($orderFields[31]) == 0) $orderFields[31] = "0";

                //

                if ($orderFields[1] == 'PRIMO') // Order Line
                  {
                  $orderLine['fileName'] = $thisFile;
                  $orderLine['text'] = $line;

                  $orderLine['ISIN'] = trim($orderFields[4]);
                  $orderLine['account'] = trim($orderFields[8]);
                  $orderLine['orderDate'] = date('Y-m-d', strtotime(trim($orderFields[9]))); // Date in the format yyyymmdd, so strtotime() works OK.
                  $orderLine['settlementDate'] = date('Y-m-d', strtotime(trim($orderFields[10])));
                  $orderLine['unitCurrency'] = trim($orderFields[11]);
                  $orderLine['priceType'] = trim($orderFields[12]);
                  $orderLine['priceDate'] = date('Y-m-d', strtotime(trim($orderFields[13])));
                  $orderLine['price'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[14]));
                  $orderLine['priceCurrency'] = trim($orderFields[15]);
                  $orderLine['lineType'] = trim($orderFields[16]); // 'Ordres', 'Recap', 'Net'

                  $orderLine['originatorID'] = trim($orderFields[17]);
                  $orderLine['originatorType'] = trim($orderFields[19]);
                  $orderLine['clientID'] = trim($orderFields[20]);
                  $orderLine['clientType'] = trim($orderFields[19]);

                  $orderLine['orderType'] = trim($orderFields[25]); // 'SUBS', 'REDM'
                  $orderLine['quantity'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[29]));
                  $orderLine['amount'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[30]));
                  $orderLine['settlement'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[31]));

                  $orders_array[] = $orderLine;
                  }

                }

              } // $line

            } // If Count($lines)
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
            }

          // Save Confirmation file entries.
          $DoMoveFile = true;

          if (count($orders_array) > 0)
            {
            if (set_SubscriptionBNPParis_sqlsrv($orders_array) === false)
              {
              $DoMoveFile = false;
              }
            }
          }


        // Move Subscriptions / Redemptions file to Processed directory.

        if ($DoMoveFile)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

          try
            {
            $nowDate = New DateTime();
            }
          catch (Exception $e)
            {
            // In the case of an exception, probably because the timezone is not set in php.ini, default to Paris.
            date_default_timezone_set('Europe/Paris');
            $nowDate = New DateTime();
            }

          $newFile = $nowDate->format('YmdHis') . '_' . $thisFile;

          $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
          if ($copyResult)
            {
            $capture = array();
            $capture['setID'] = 8;
            $capture['result'] = 'success';
            $capture['filename'] = $thisFile;
            $capture['priority'] = 0;
            $sql_DateEntered = get_DateNow_sqlsvr();
            $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

            unlink(($pendingDirectoryName . '/' . $thisFile));
            }
          else
            {
            	$capture = array();
            	$capture['setID'] = 8;
            	$capture['result'] = 'failed to copy';
            	$capture['filename'] = $thisFile;
            	$capture['priority'] = 0;
            	$sql_DateEntered = get_DateNow_sqlsvr();
            	$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
            }

          }
        else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
          }

        } // If preg_match()
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
        }

      } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
  else
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

  if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  capturefailed('checkSubscriptionsBNPParis, failed to process file : ' . $e->getMessage(), $thisFile);
  }






?>
