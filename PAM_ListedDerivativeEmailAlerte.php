<?php
/**
 * Created by PhpStorm.
 * User: rruzinda
 * Date: 5/18/14
 * Time: 10:23 AM
 */
require_once('localise/localise.php');
require_once('functions/db_functions.php');
require_once(LOGIN_PASSWORD_FILE);
define('DELIMITER', ';');

$timestamp = date('YmdHis');

//ini_set('display_errors', 1);
$sqlsvr_conn = renaissance_connect();

//declare the SQL statement that will query the database
//$query = mssql_query($query_candidates); status 1 = Validated MO
$query = mssql_query("SELECT txn.TransactionParentID,txn.TransactionID,
fund.FundName,
bkr.BrokerDescription,
ttype.TransactionType,
instr.InstrumentDescription,
instr.InstrumentExchangeCode,
txn.TransactionSignedUnits,
txn.TransactionPrice
FROM fn_tblTransactionSingle_SelectKD(0,null,null) AS txn,
fn_tblInstrument_SelectKD(null) AS instr,
fn_tblBroker_SelectKD(null) AS bkr,
fn_tblTransactionType_SelectKD(null) AS ttype,
fn_tblFund_SelectKD(null) as fund
WHERE
txn.TransactionInstrument = instr.InstrumentID
and txn.TransactionBroker = bkr.BrokerID
and txn.TransactionType = ttype.TransactionTypeID
and txn.TransactionFund = fund.FundID
and txn.TransactionTradeStatusID = 14
and txn.TransactionType in (4,5)
and txn.TransactionDateEntered > '2014-05-01'
and not exists ( SELECT tradefile.TransactionParentID FROM tblTradeFileExecutionReporting AS tradeFile
WHERE  tradefile.TransactionParentID = txn.TransactionParentID)
and instr.InstrumentType in (8,15)
and bkr.EMSX_Broker_ID in ('XANE','SGLD')");


$linesNbr = mssql_num_rows($query);



if (!mssql_num_rows($query)) {
    echo 'No candidates found' . PHP_EOL;
} else {


    while ($row = mssql_fetch_array($query)) {
        //echo $row['TransactionParentID'] . " - " . $row['TransactionID']   . PHP_EOL;
        //print_r($row);

        if (count($row) > 0)
        {
            //echo $row['TransactionParentID'].PHP_EOL;
            //echo $row['TransactionID'].PHP_EOL;
            $subject = $row['FundName'] . " - Transaction Derives listes via " . $row['BrokerDescription'] . " - " . $timestamp;

            $message = "";
            $message = $row['FundName'] . " / ";
            $message .= $row['BrokerDescription'] . " / ";
            $message .= $row['TransactionType'] . " / ";
            $message .= $row['InstrumentDescription'] . " / ";
            $message .= round($row['TransactionSignedUnits'],4) . " / ";
            $message .= round($row['TransactionPrice'],4) ;

            echo $message . PHP_EOL;


            $headers = "From: ITSUPPORTAM@primonial.fr";
            $mailto = "rodolphe.ruzindana@primonial.fr,mo_pam@primonial.fr,lotfi.ouederni@primonial.fr";
            //$mailto = "rodolphe.ruzindana@primonial.fr";
            //$message = wordwrap($message, 70, "\r\n");

            // SendMail
            mail($mailto, $subject, $message, $headers);

             $reportTable = array();

             $reportTable['AdministratorID'] = 0;
             $reportTable['TransactionParentID'] = $row['TransactionParentID'];
             $reportTable['TransactionID'] = $row['TransactionID'];
             $reportTable['TradeReportString'] = $message;
             // Entry in tblTradeFileExecutionReporting table
             insertTradeFileExecution($reportTable);

             // The entry has been inserted in the executions file
             set_execDone_sqlsrv($reportTable);


        }


    }


}

mssql_free_result($query);





function get_Execs_sqlsrv($recID){

    try
    {

        //ini_set('display_errors', 1);
        $sqlsvr_conn = renaissance_connect();


        // Create a new stored prodecure
        $stmt = mssql_init('zzz_createExecutionsFile', $sqlsvr_conn);
        $knowledgeDate_Str = Null;
        mssql_bind($stmt, '@AuditID', &$recID, SQLINT4, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate_Str , SQLVARCHAR, false, false, 20);


        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}


function insertTradeFileExecution($results){


    try
    {

        $results_array = array();

        //print_r($results);

        $sqlsvr_conn = renaissance_connect();


        $AdministratorID = $results['AdministratorID'];
        $transactionID = $results['TransactionID'];
        $transactionParentID = $results['TransactionParentID'];
        $tradeReportString = $results['TradeReportString'];
        $knowledgeDate = Null;

        // Create a new stored prodecure
        $stmt = mssql_init('adp_tblTradeFileExecutionReporting_InsertCommand', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {


            mssql_bind($stmt, '@AdministratorID', &$AdministratorID, SQLINT4, false, false);
            mssql_bind($stmt, '@TransactionID', &$transactionID, SQLINT4, false, false);
            mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);
            mssql_bind($stmt, '@TradeReportString', &$tradeReportString , SQLVARCHAR, false, false, 2000);
            mssql_bind($stmt, '@KnowledgeDate', &$$knowledgeDate ,SQLVARCHAR , false, false);



            mssql_execute($stmt);

            //if (mssql_num_rows($proc_result) > 0)
            //{
            //    $row = mssql_fetch_assoc($proc_result);
            //
            //    $results_array[$count++] = $row;
            //}

            // Free statement
            mssql_free_statement($stmt);
        }

     // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_execDone_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN', 'filename' and TransactionParentID
    // do whatever magic is required to change the status of execs in Venice

    try
    {

        $results_array = array();


        $sqlsvr_conn = renaissance_connect();
        //ini_set('display_errors', 1);


            $UserID = 0;
            $Token  = 'Null';
            $transactionParentID = $results['TransactionParentID'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_tblTradeFileExecutionReporting_Delete', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
                mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
                mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);

                mssql_execute($stmt);

                //if (mssql_num_rows($proc_result) > 0)
                //{
                //    $row = mssql_fetch_assoc($proc_result);
                //
                //    $results_array[$count++] = $row;
                //}

                // Free statement
                mssql_free_statement($stmt);
            }



    } catch (Exception $e)
    {
    }

    return $results_array;


}