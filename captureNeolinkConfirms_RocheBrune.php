<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

if (NEOCAPTURE_DEBUG_ECHO) echo 'CaptureNeolinkConfirms_RocheBrune, Start' . "<br>". PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_confirmations.php');

$names = array();
  $names[] = 'neo_bank';
  $names[] = 'neo_st';
  $names[] = 'neo_status';
  $names[] = 'neo_cat';
  $names[] = 'neo_t';
  $names[] = 'neo_settlement';
  $names[] = 'neo_clientreference';
  $names[] = 'neo_quantity';
  $names[] = 'neo_unit';
  $names[] = 'neo_isin';
  $names[] = 'neo_securitydescription';
  $names[] = 'neo_settlementamount';
  $names[] = 'neo_settlementcurrency';
  $names[] = 'neo_price';
  $names[] = 'neo_pricecurrency';
  $names[] = 'neo_blank1';
  $names[] = 'neo_trade';
  $names[] = 'neo_securitiesaccount';
  $names[] = 'neo_accountdescription';
  $names[] = 'neo_settagentbic';
  $names[] = 'neo_settagentdescr';
  $names[] = 'neo_ctrpbic';
  $names[] = 'neo_tradecounterpartdescr';
  $names[] = 'neo_bankreference';
  $names[] = 'neo_country';
  $names[] = 'neo_local';

  $lastfile="";
  $lastfile = file_get_contents("/mnt/neolink/TradeLogRocheBrune.txt");
  
  if (NEOCAPTURE_DEBUG_ECHO) echo ('Lastfile : '. $lastfile) . "<br>".PHP_EOL;

try
{
	
	$filecontents="";
	$filecontents = file_get_contents(trim($lastfile));
	
	$lines = explode("\n", $filecontents);

	if (NEOCAPTURE_DEBUG_ECHO) echo ('File lines : '. count($lines)) . "<br>".PHP_EOL;

	// Process lines :
	
	$sql_DateEntered = get_DateNow_sqlsvr();
		
	$id = 0;
		
	$capture = array();
	$capture['setID'] = 31;
	$capture['dateandtime'] = convertToSQLDate(time());
	$capture['filename']=$lastfile;
	$capture['result'] = 'success';
	$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
	
	if (NEOCAPTURE_DEBUG_ECHO) echo '  $id        : '.$id . "<br>".PHP_EOL;
	if (NEOCAPTURE_DEBUG_ECHO) echo '  $sqlsvr_id : '.$sqlsvr_id . "<br>".PHP_EOL;
	
	$confirmation=array();

	foreach ($lines as $line)
	{
		if (strlen($line) > 20)
		{

			try
			{

				// do processing
				
				if (NEOCAPTURE_DEBUG_ECHO) echo ($line) . "<br>". PHP_EOL;
				
				$elements=explode("\t",$line);
				
				$i=0;
				foreach ($elements as $element){
					if ($i<count($names)){
						$confirmation[$names[$i]] = $element;
					}
					$i++;
				}
				 

			} catch (Exception $e)
			{
				if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . "<br>".PHP_EOL;
				capturefailed('Tradefile, failed to save trade : ' . $e->getMessage(), $thisFile);
			}

		}
		
		// write the confirmation to neocapture
		
		if (count($confirmation)>20){
			
			$confirmation['captureID'] = $sqlsvr_id;
			$confirmation['instance']='RB';
			
			echo var_dump($confirmation). "<br>".PHP_EOL;
			
			add_confirmations_sqlsrv($confirmation, $sql_DateEntered);
			
		}
							
	}
	} catch (Exception $e)
	{
		if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . "<br>". PHP_EOL;
		//capturefailed('Confirm file, failed to process file : ' . $e->getMessage(), $thisFile);
	}


?>