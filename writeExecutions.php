<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'WriteExecutions, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');

// Generate Trade file name

$date = date('dmYHis');
$timestampFilename = date('YmdHis');
$timestampContent = date('YmdHis');
$header = 'BOF;' . $timestampContent . ';PAM_FR';



$filename = EXECUTIONS_FILE_DIRECTORY . '/' . EXECUTIONS_FILE_NAME . '_' . $timestampFilename . '.csv';

echo "<h1> Filename : " .  $filename . "</h1>";
echo "<h1> Header : " . $header . "</h1>";

if (NEOCAPTURE_DEBUG_ECHO) echo 'Write Execution file.' . PHP_EOL;

// skip if execution file already exists

if (file_exists($filename))
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  Trade file already exists. Abort and try later.' . PHP_EOL;
  } else
  {

  // Get trades from sqlsrv

  $SecondsSinceUpdate = EXECUTIONS_FILE_DELAY_SECONDS;

  $execsArray = get_Executions_sqlsrv($SecondsSinceUpdate);

  $fileContents = "";
  $results = array();
  $line = array();

  $linesNbr = count($execsArray);
  $trailer = (string)$linesNbr . ';EOF';


  echo "<h1> Trailer : " . $trailer . "</h1>";


  // Process pending execs (if there are any).

  if (count($execsArray) > 0)
    {

    $fileContents .= $header . "\r\n";
    foreach ($execsArray as $execution)
      {

      $fileContents .= $execution['TradeReportString'] . "\r\n";

      $line['RN'] = $execution['RN'];
      $line['filename'] = $filename;
      $line['TransactionParentID'] = $execution['TransactionParentID'];
      $results[] = $line;

      echo "<h1>" . $execution['TradeReportString'] . "</h1>";

      if (NEOCAPTURE_DEBUG_ECHO) echo '  Writing to file : ' . strval($execution['RN']) . ', `' . $execution['TradeReportString'] . '`' . PHP_EOL;

      }
    $fileContents .= $trailer. "\r\n";

    if (!(file_put_contents($filename, $fileContents) === false))
      {
      // exec sucess
       $results = set_execsFileDone_sqlsrv($results);


      if (NEOCAPTURE_DEBUG_ECHO)
        {
        foreach ($results as $line)
          {
          echo '  Confirmed in DB : RN = ' . strval($line['RN']) . PHP_EOL;
          capturemessage('success');
          }
        }
      } else
      {
      // trade fail
      if (NEOCAPTURE_DEBUG_ECHO) echo 'Write to file FAILED.' . PHP_EOL;
          capturemessage('Write to file failed');
          echo 'Write to file failed';

      }
    } else
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  No execs found.' . PHP_EOL;
        capturemessage('No execs found');
        echo 'No execs found';
    }

  }

if (NEOCAPTURE_DEBUG_ECHO) echo '  Write Execs files - END.' . PHP_EOL;

function capturemessage($message)
{
	$capture = array();
	$capture['setID'] = 15;
	$capture['dateandtime'] = convertToSQLDate(time());
	$capture['result'] = $message;

	$sql_DateEntered = get_DateNow_sqlsvr();
	add_capture_sqlserver($capture, $sql_DateEntered);
}

?>