<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);


require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);
require_once 'includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconcilliation_dashboard.php');

//The following list must be updated when a new fund is created for AltaRocca
$altaRoccaFunds = "1310701";

if (NEOCAPTURE_DEBUG_ECHO) echo 'check_Inventory_AltaRocca.php, Start' . PHP_EOL;

ini_set('display_errors', 1);


$pendingDirectoryName = VLFILES;
$processedDirectoryName = VLFILES_PROCESSED;
$filePattern = INVENTORY_PATTERN;
$directoryFiles = false;

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;


try
{
    $directoryFiles = scandir($pendingDirectoryName);
}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
}

try
{
    $sql_DateEntered = get_DateNow_sqlsvr();
    $sql_DateEntered = get_DateNow_sqlsvr();
    $capture = array();
    $capture['setID'] = 19;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $id = add_capture($capture);
    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if ($directoryFiles !== false)
    {
        foreach ($directoryFiles as $thisFile)
        {
            // is this file is a directory, especially . and .


            if (is_dir($pendingDirectoryName . '/' . $thisFile))
            {
                continue;
            }

            if (preg_match($filePattern, $thisFile))
            {
                echo 'Filename : ' . $thisFile . PHP_EOL;
                if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

                $objPHPExcel = PHPExcel_IOFactory::load($pendingDirectoryName . '/' . $thisFile);

                if ($objPHPExcel === false)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to load file ' . $thisFile) . PHP_EOL;

                    $DoMoveFile = false;
                }
                else
                {
                    $sheet = $objPHPExcel->getSheet(0);
                    $firstFundCode = $sheet->getCell('A2')->getValue();
                    $firstFundName = $sheet->getCell('B2')->getValue();

                    $lastRow = $sheet->getHighestRow();
                    echo '---------------------------------' . PHP_EOL;
                    echo 'First FundCode : ' . $firstFundCode . PHP_EOL;

                    $params = array();

                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has lines.', count($lastRow)) . PHP_EOL;

                    $fundPattern = '/'. $firstFundCode . '/';

                    //if (strpos($altaRoccaFunds,$fundCode) >= 0)
                    if (preg_match($fundPattern,$altaRoccaFunds) and substr($firstFundName,0,5) == 'ALTAR')
                    {

                        echo 'AltaRocca valid inventory file --- processing ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;


                        // Inventory
                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Exchange Rate '. PHP_EOL;
                        xls_inventory_table($sheet, $sqlsvr_id);


                        // Save events file entries.
                        $DoMoveFile = true;



                    } // If Count($lines)
                    else
                    {
                        echo 'not interesting or not for AltaRocca ---- skip the file ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';

                        $DoMoveFile = false;

                    }


                }

                if ($DoMoveFile)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

                    try
                    {
                        $nowDate = New DateTime();
                    }
                    catch (Exception $e)
                    {
                        // In the case of an exception, probable because the timezone is not set in php.ini, default to Paris.
                        date_default_timezone_set('Europe/Paris');
                        $nowDate = New DateTime();
                    }

                    $newFile = $nowDate->format('YmdHis') . '_Inventory_AR_' . $thisFile;

                    $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
                    if ($copyResult)
                    {
                        unlink(($pendingDirectoryName . '/' . $thisFile));
                    }
                    else
                    {
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
                    }

                }
                else
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
                }

            } // If preg_match()
            else
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
            }

        } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
    else
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
    capturefailed('check_Inventory_AltaRocca : ' . $e->getMessage(), $thisFile);
}




function xls_inventory_table($currentSheet, $sqlsvr_id)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `inventory sheet`' . PHP_EOL;


    try
    {

        $rowNbr = 1;


        foreach($currentSheet->getRowIterator() as $row) {

            // in this sheet the currency are listed after the 9th line
            if ($rowNbr >= 2)
            {
                $inventoryLine = array();
                /*
                    $names[] = array('neo_checkall', 0, 0, 50);
                    $names[] = array('neo_instrumenttype', 0, 0, 50);
                    $names[] = array('neo_sessionstatus', 0, 0, 50);
                    $names[] = array('neo_isin', 0, 0, 50);
                    $names[] = array('neo_description', 0, 0, 255);
                    $names[] = array('neo_quantity', 0, 0, 50);
                    $names[] = array('neo_type', 0, 0, 50);
                    $names[] = array('neo_pricedate', 0, 0, 50);
                    $names[] = array('neo_price', 0, 0, 50);
                    $names[] = array('neo_priceflag', 0, 0, 50);
                    $names[] = array('neo_quotecurrency', 0, 0, 5);
                    $names[] = array('neo_marketvalue_qc', 0, 0, 50);
                    $names[] = array('neo_unitprice_qc', 0, 0, 50);
                    $names[] = array('neo_exchangerate', 0, 0, 50);
                    $names[] = array('neo_referencecurrency', 0, 0, 50);
                    $names[] = array('neo_marketvalue_rc', 0, 0, 50);
                    $names[] = array('neo_accruedinterest_rc', 0, 0, 50);
                    $names[] = array('neo_costprice_rc', 0, 0, 50);
                    $names[] = array('neo_nonrealisedgainloss_rc', 0, 0, 50);
                    $names[] = array('neo_percentnetassets', 0, 0, 50);
                    $names[] = array('fundcode', 0, 0, 50);
                    $names[] = array('neo_realisedgainloss_qc', 0, 0, 50);
                    $names[] = array('neo_instrumentweight', 0, 0, 50);
                    $names[] = array('neo_assettype', 0, 0, 50);
                    $names[] = array('neo_subassettype', 0, 0, 50);
                    $names[] = array('neo_subassettypedesc', 0, 1, 255);
                    $names[] = array('neo_issuercode', 0, 0, 50);
                    $names[] = array('neo_quotationplace', 0, 0, 50);
                    $names[] = array('neo_quotationplacedesc', 0, 1, 255);
                    $names[] = array('neo_cotationcountry', 0, 0, 50);
                    $names[] = array('neo_maturitydate', 0, 0, 50);
                    $names[] = array('neo_nominal', 0, 0, 50);
                    $names[] = array('neo_underlyingassettype', 0, 0, 50);
                    $names[] = array('neo_underlyingassettypedesc', 0, 1, 255);
                    $names[] = array('neo_quotite', 0, 0, 50);
                    $names[] = array('neo_strike', 0, 0, 50);

                 */

                $inventoryLine['captureID'] = $sqlsvr_id;
                $inventoryLine['NAVDate'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();;
                $inventoryLine['checkall'] = '';
                $inventoryLine['sharetype'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();
                $inventoryLine['sharetype'] = $inventoryLine['sharetype'] . '/' . $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $inventoryLine['sessionstatus'] = '';
                $inventoryLine['isin'] = $currentSheet->getCell('E' . (string)$rowNbr)->getValue();
                $inventoryLine['description'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $inventoryLine['quantity'] = $currentSheet->getCell('M' . (string)$rowNbr)->getValue();
                $inventoryLine['type'] = '';
                $inventoryLine['pricedate'] = $currentSheet->getCell('U' . (string)$rowNbr)->getValue();
                $inventoryLine['price'] = $currentSheet->getCell('N' . (string)$rowNbr)->getValue();
                $inventoryLine['priceflag'] = '';
                $inventoryLine['quotecurrency'] = $currentSheet->getCell('Q' . (string)$rowNbr)->getValue();
                $inventoryLine['marketvalueqc'] = $currentSheet->getCell('S' . (string)$rowNbr)->getValue();
                $inventoryLine['unitpriceqc'] = $currentSheet->getCell('P' . (string)$rowNbr)->getValue();
                $inventoryLine['exchangerate'] = $currentSheet->getCell('AC' . (string)$rowNbr)->getValue();
                $inventoryLine['referencecurrency'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $inventoryLine['marketvaluerc'] = $currentSheet->getCell('W' . (string)$rowNbr)->getValue();
                $inventoryLine['accruedinterestrc'] = $currentSheet->getCell('Z' . (string)$rowNbr)->getValue();
                $inventoryLine['costpricerc'] = $currentSheet->getCell('P' . (string)$rowNbr)->getValue();
                $inventoryLine['nonrealisedgainlossrc'] = $currentSheet->getCell('X' . (string)$rowNbr)->getValue();
                $inventoryLine['percentnetassets'] = $currentSheet->getCell('AE' . (string)$rowNbr)->getValue();
                $inventoryLine['fundcode'] = $currentSheet->getCell('A' . (string)$rowNbr)->getValue();
                $inventoryLine['realisedgainlossqc'] = $currentSheet->getCell('W' . (string)$rowNbr)->getValue();
                $inventoryLine['instrumentweight'] = $currentSheet->getCell('AA' . (string)$rowNbr)->getValue();
                $inventoryLine['assettype'] = $currentSheet->getCell('AN' . (string)$rowNbr)->getValue();
                $inventoryLine['subassettype'] = $currentSheet->getCell('AL' . (string)$rowNbr)->getValue(); //to be checked
                $inventoryLine['subassettypedesc'] = $currentSheet->getCell('AL' . (string)$rowNbr)->getValue(); //to be checked
                $inventoryLine['issuercode'] = $currentSheet->getCell('AJ' . (string)$rowNbr)->getValue();
                $inventoryLine['quotationplace'] = $currentSheet->getCell('K' . (string)$rowNbr)->getValue();
                $inventoryLine['quotationplacedesc'] = $currentSheet->getCell('K' . (string)$rowNbr)->getValue();
                $inventoryLine['cotationcountry'] = $currentSheet->getCell('J' . (string)$rowNbr)->getValue();
                $inventoryLine['maturitydate'] = $currentSheet->getCell('L' . (string)$rowNbr)->getValue();
                $inventoryLine['nominal'] = $currentSheet->getCell('AF' . (string)$rowNbr)->getValue(); //diff with quotite ??
                $inventoryLine['underlyingassettype'] = $currentSheet->getCell('AT' . (string)$rowNbr)->getValue();
                $inventoryLine['underlyingassettypedesc'] = $currentSheet->getCell('AU' . (string)$rowNbr)->getValue();
                $inventoryLine['quotite'] = $currentSheet->getCell('AF' . (string)$rowNbr)->getValue();
                $inventoryLine['strike'] = '';

                // Change date format
                $pdt = PHPExcel_Shared_Date::ExcelToPHP($inventoryLine['pricedate']);
                $pdt = date('d/m/Y', $pdt);
                $inventoryLine['pricedate'] = $pdt;

                $ndt = PHPExcel_Shared_Date::ExcelToPHP($inventoryLine['NAVDate']);
                $ndt = date('d/m/Y', $ndt);
                $inventoryLine['NAVDate'] = $ndt;

                if ($inventoryLine['maturitydate'])
                {
                    $mdt = PHPExcel_Shared_Date::ExcelToPHP($inventoryLine['maturitydate']);
                    $mdt = date('d/m/Y', $mdt);
                    $inventoryLine['maturitydate'] = $mdt;
                }

                if ($inventoryLine['marketvaluerc'])
                {

                    // We assume that BNP send a clean price in the price column and we want a dirty price
                    $newPrice = $inventoryLine['price'] * ( $inventoryLine['marketvaluerc'] / ($inventoryLine['marketvaluerc'] - $inventoryLine['accruedinterestrc']));

                        //echo  "Old Price = " . $inventoryLine['price'] . " New price = " . $newPrice . PHP_EOL;
                    $inventoryLine['price'] = (string)$newPrice;


                }

                if ($inventoryLine['isin'])
                {
                    //echo 'FUND CODE  : ' . $inventoryLine['fundcode'] . PHP_EOL;
                    //echo 'ISIN  : ' . $inventoryLine['isin'] . PHP_EOL;
                    //echo 'DESCR  : ' . $inventoryLine['description'] . PHP_EOL;
                    insert_inventory_sqlsrv($inventoryLine);
                }else
                {
                    break;
                }

            }

            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}
function insert_inventory_sqlsrv($params)
{
    try
    {
        $sql_DateEntered = get_DateNow_sqlsvr();
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_inventory_InsertCommand', $sqlsvr_conn);



        /*
            PROCEDURE [dbo].[adp_rec_inventory_InsertCommand]
            (
            @IDENTITY int OUTPUT,
            @captureID int,
            @NAVDate [nvarchar](50),
            @fundcode nvarchar(50),
            @neo_checkall nvarchar(50),
            @neo_instrumenttype nvarchar(50),
            @neo_sessionstatus nvarchar(50),
            @neo_isin nvarchar(50),
            @neo_description nvarchar(255),
            @neo_quantity nvarchar(50),
            @neo_type nvarchar(50),
            @neo_pricedate nvarchar(50),
            @neo_price nvarchar(50),
            @neo_priceflag nvarchar(50),
            @neo_quotecurrency nvarchar(5),
            @neo_marketvalue_qc nvarchar(50),
            @neo_unitprice_qc nvarchar(50),
            @neo_exchangerate nvarchar(50),
            @neo_referencecurrency nvarchar(5),
            @neo_marketvalue_rc nvarchar(50),
            @neo_accruedinterest_rc nvarchar(50),
            @neo_costprice_rc nvarchar(50),
            @neo_nonrealisedgainloss_rc nvarchar(50),
            @neo_percentnetassets nvarchar(50),
            @neo_realisedgainloss_qc nvarchar(50),
            @neo_instrumentweight nvarchar(50),
            @neo_assettype nvarchar(50),
            @neo_subassettype nvarchar(50),
            @neo_subassettypedesc nvarchar(255),
            @neo_issuercode nvarchar(50),
            @neo_quotationplace nvarchar(50),
            @neo_quotationplacedesc nvarchar(255),
            @neo_cotationcountry nvarchar(50),
            @neo_maturitydate nvarchar(50),
            @neo_nominal nvarchar(50),
            @neo_underlyingassettype nvarchar(50),
            @neo_underlyingassettypedesc nvarchar(255),
            @neo_quotite nvarchar(50),
            @neo_strike nvarchar(50),
            @neo_DateEntered nvarchar(50)
            )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@fundcode', &$params['fundcode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_checkall', &$params['checkall'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_instrumenttype', &$params['sharetype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_sessionstatus', &$params['sessionstatus'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_isin', &$params['isin'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_description', &$params['description'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quantity', &$params['quantity'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_type', &$params['type'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_pricedate', &$params['pricedate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_price', &$params['price'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_priceflag', &$params['priceflag'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quotecurrency', &$params['quotecurrency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_marketvalue_qc', &$params['marketvalueqc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_unitprice_qc', &$params['unitpriceqc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_exchangerate', &$params['exchangerate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_referencecurrency', &$params['referencecurrency'], SQLVARCHAR, false, false, 5);
        mssql_bind($stmt, '@neo_marketvalue_rc', &$params['marketvaluerc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_accruedinterest_rc', &$params['accruedinterestrc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_costprice_rc', &$params['costpricerc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_nonrealisedgainloss_rc', &$params['nonrealisedgainlossrc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_percentnetassets', &$params['percentnetassets'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_realisedgainloss_qc', &$params['realisedgainlossqc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_instrumentweight', &$params['instrumentweight'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_assettype', &$params['assettype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_subassettype', &$params['subassettype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_subassettypedesc', &$params['subassettypedesc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_issuercode', &$params['issuercode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quotationplace', &$params['quotationplace'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quotationplacedesc', &$params['quotationplacedesc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_cotationcountry', &$params['cotationcountry'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_maturitydate', &$params['maturitydate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_nominal', &$params['nominal'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_underlyingassettype', &$params['underlyingassettype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_underlyingassettypedesc', &$params['underlyingassettypedesc'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_quotite', &$params['quotite'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_strike', &$params['strike'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}

function capturefailed($message)
{
    $capture = array();
    $capture['setID'] = 19;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = $message;
    $sql_DateEntered = get_DateNow_sqlsvr();
    add_capture_sqlserver($capture, $sql_DateEntered);
}

?>
