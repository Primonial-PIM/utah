<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_morningstar_prices.php');


//debug info
function LiveOutput($tag, $message)
  {
  $channel = "d994fTXc";
  file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
  }

function getMorningstarIndexReturns($msCodes)
  {

  $results = array();
  $resultline = array();

  foreach ($msCodes AS $instrumentLine)
    {

    $benchmarkid = trim($instrumentLine['Morningstar']);
    $instrumentid = $instrumentLine['ID'];
    if (array_key_exists('MonthsToGet', $instrumentLine))
      {
      $monthsToGet = $instrumentLine['MonthsToGet'];
      } else
      {
      $monthsToGet = '1';
      }

    if (strlen($benchmarkid) > 0)
      {
      $url = 'http://lt.morningstar.com/api/rest.svc/timeseries_cumulativereturn/jrhe6v87w5?type=MSID&timeperiod=' . $monthsToGet . '&frequency=daily&outputType=XML&id=' . $benchmarkid . ']8]0]CAALL$$ALL';

      if (NEOCAPTURE_DEBUG_ECHO) echo ($url) . "<br>";

      try
        {

        $xmlretstring = (getGeneric($url));
        //echo $fund."<br>";
        $returnContents = "";
        $xmlret = simplexml_load_string($xmlretstring);

        $i = 0;
        $lastout = 100; /* Not for any reason, just stops code-sense errors. */

        foreach ($xmlret->Security->CumulativeReturnSeries->HistoryDetail as $History)
          {
          $hdate = $History->EndDate;
          $hvalue = $History->Value;
          $hdateparts = explode("-", $hdate);
          $outvalue = (float)$hvalue + 100.00;
          $outdate = $hdateparts[0] . "-" . $hdateparts[1] . "-" . $hdateparts[2]; // yyyy-mm-dd (Universally recognised)
          $returnContents .= $benchmarkid . "," . $outvalue . "," . $outdate . "\r\n";
          $i++;

          if ($i > 1)
            {
            $return = $outvalue / $lastout - 1;
            $resultline['ID'] = $instrumentid;
            $resultline['Morningstar'] = $benchmarkid;
            $resultline['date'] = $outdate;
            $resultline['return'] = $return;
            $results[] = $resultline;
            //LiveOutput("return",$benchmarkid." ".$outdate." ".$return);
            }

          $lastout = $outvalue;

          } // for

        } catch (Exception $e)
        {
        capturefailed('capture failed ' . $e->getMessage());
        return "failed";
        }

      } // if (strlen($benchmarkid) > 0)

    }

  return $results;
  }

// MAIN

if (NEOCAPTURE_DEBUG_ECHO) echo 'Morningstar Index Returns, Start' . PHP_EOL;

if (NEOCAPTURE_DEBUG_ECHO) echo '  Get Index IDs from Venice' . PHP_EOL;

$msCodes = get_morningstar_index_list_sqlsrv();

if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . count($msCodes[0]) . ' Instruments returned.' . PHP_EOL;

$results = GetMorningstarIndexReturns($msCodes);

if (count($results) > 1)
  {
  $success = true;
  } else
  {
  $success = false;
  }


if ($success)
  {

  try
    {

    $capture = array();
    $capture['setID'] = 11;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $sql_DateEntered = get_DateNow_sqlsvr();

    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Capture ID : ' . $sqlsvr_id . PHP_EOL;

    $results_array = add_morningstar_index_returns_sqlsrv($msCodes, $results, $sql_DateEntered);

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo 'write failed ' . $e->getMessage() . PHP_EOL;

    //capturefailed('write failed ' . $e->getMessage());
    exit;
    }

  } else
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo 'capture failed';
  //capturefailed('failed');
  exit;
  }

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>