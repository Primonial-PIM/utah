<?php

if ((!defined('NEOCAPTURE_ROOT')) || (strlen(NEOCAPTURE_ROOT) <= 0))
{
    define('NEOCAPTURE_ROOT', '/var/www/utah');
}

if ((!defined('LOGIN_PASSWORD_FILE')) || (strlen(NEOCAPTURE_ROOT) <= 0))
{
    define('LOGIN_PASSWORD_FILE', '/var/privatephp/FundStatConnectLive.php');
}

if ((!defined('NEOCAPTURE_TRADEFILES_ROOT')) || (strlen(NEOCAPTURE_TRADEFILES_ROOT) <= 0))
{
    define('NEOCAPTURE_TRADEFILES_ROOT', '/mnt/export_opcvm');
}

if ((!defined('NEOCAPTURE_LOG_TO_FILE')) || (strlen(NEOCAPTURE_LOG_TO_FILE) <= 0))
{
    define('NEOCAPTURE_LOG_TO_FILE', false);
}

if ((!defined('NEOCAPTURE_DEBUG_ECHO')) || (strlen(NEOCAPTURE_DEBUG_ECHO) <= 0))
{
    define('NEOCAPTURE_DEBUG_ECHO', true);
}

// BNP Paris Trade file constants

if ((!defined('PARIS_TRADEFILE_NAME')) || (strlen(PARIS_TRADEFILE_NAME) <= 0))
  {
  define('PARIS_TRADEFILE_NAME', 'VTO30029');
  }

if ((!defined('PARIS_TRADEFILE_DIRECTORY')) || (strlen(PARIS_TRADEFILE_DIRECTORY) <= 0))
  {
  define('PARIS_TRADEFILE_DIRECTORY', '/mnt/export_opcvm/Paris');
  }

if ((!defined('PARIS_RB_TRADEFILE_DIRECTORY')) || (strlen(PARIS_RB_TRADEFILE_DIRECTORY) <= 0))
{
    define('PARIS_RB_TRADEFILE_DIRECTORY', '/mnt/export_opcvm/ParisRB');
}

if ((!defined('PARIS_TRADEFILE_DELAY_SECONDS')) || (strlen(PARIS_TRADEFILE_DELAY_SECONDS) <= 0))
  {
  define('PARIS_TRADEFILE_DELAY_SECONDS', 60);
  }

if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT) <= 0))
  {
  define('PARIS_TRADEFILES_ACKNOWLEDGEMENT', '/mnt/acknowledgements/bp2s');
  }

if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED) <= 0))
  {
  define('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED', '/mnt/acknowledgements/bp2s/processed');
  }

if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  define('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN', '#^[a-zA-Z0-9_\.]+\.VTO5226W\.[a-zA-Z0-9_\.]+$#');
  }

if ((!defined('PARIS_TRADEFILES_LOCALE')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  define('PARIS_TRADEFILES_LOCALE', 'fr-FR');
  }

// BNP Luxembourg Trade file constants

if ((!defined('BNP_LUX_TRADEFILE_NAME')) || (strlen(BNP_LUX_TRADEFILE_NAME) <= 0))
  {
  define('BNP_LUX_TRADEFILE_NAME', 'PBL2403F');
  }

if ((!defined('BNP_LUX_TRADEFILE_DIRECTORY')) || (strlen(BNP_LUX_TRADEFILE_DIRECTORY) <= 0))
  {
  define('BNP_LUX_TRADEFILE_DIRECTORY', '/mnt/export_opcvm/Lux');
  }

if ((!defined('BNP_LUX_TRADEFILE_DELAY_SECONDS')) || (strlen(BNP_LUX_TRADEFILE_DELAY_SECONDS) <= 0))
  {
  define('BNP_LUX_TRADEFILE_DELAY_SECONDS', 60);
  }

if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT) <= 0))
  {
  define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT', '/mnt/acknowledgements/bp2s');
  }

if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED) <= 0))
  {
  define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED', '/mnt/acknowledgements/bp2s/processed');
  }

if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN', '#^[a-zA-Z0-9_\.]+\.UPLO_ALL001[a-zA-Z0-9_\.]+$#');
  }

if ((!defined('LUX_TRADEFILES_LOCALE')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  define('LUX_TRADEFILES_LOCALE', 'en-US');
  }

// BNP Paris CONFIRMATION file constants


if ((!defined('PARIS_CONFIRMATIONFILES_LOCALE')) || (strlen(PARIS_CONFIRMATIONFILES_LOCALE) <= 0))
  {
  define('PARIS_CONFIRMATIONFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
  }

if ((!defined('PARIS_CONFIRMATIONFILES')) || (strlen(PARIS_CONFIRMATIONFILES) <= 0))
  {
  define('PARIS_CONFIRMATIONFILES', '/mnt/confirms/bp2s');
  }

if ((!defined('PARIS_CONFIRMATIONFILES_PROCESSED')) || (strlen(PARIS_CONFIRMATIONFILES_PROCESSED) <= 0))
  {
  define('PARIS_CONFIRMATIONFILES_PROCESSED', '/mnt/confirms/bp2s/processed');
  }

if ((!defined('PARIS_CONFIRMATIONFILES_PATTERN')) || (strlen(PARIS_CONFIRMATIONFILES_PATTERN) <= 0))
  {
  define('PARIS_CONFIRMATIONFILES_PATTERN', '#[a-zA-Z0-9_\.]+CNOT_ALL[a-zA-Z0-9_\.]+\.csv$#');
  }

if ((!defined('PARIS_CONFIRMATIONFILES_RB_PATTERN')) || (strlen(PARIS_CONFIRMATIONFILES_RB_PATTERN) <= 0))
{
    define('PARIS_CONFIRMATIONFILES_RB_PATTERN', '#[a-zA-Z0-9_\.]+CNOT_[0-9]+\.csv$#');
}

if ((!defined('PARIS_CONFIRMATIONFILES_SEPARATOR')) || (strlen(PARIS_CONFIRMATIONFILES_SEPARATOR) <= 0))
  {
  define('PARIS_CONFIRMATIONFILES_SEPARATOR', ';');
  }


// BNP Paris Subscription / Redemption file constants


if ((!defined('PARIS_ORDERFILES_LOCALE')) || (strlen(PARIS_ORDERFILES_LOCALE) <= 0))
  {
  define('PARIS_ORDERFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
  }

if ((!defined('PARIS_ORDERFILES')) || (strlen(PARIS_ORDERFILES) <= 0))
  {
  define('PARIS_ORDERFILES', '/mnt/SR/Insert_SR');
  }

if ((!defined('PARIS_ORDERFILES_PROCESSED')) || (strlen(PARIS_ORDERFILES_PROCESSED) <= 0))
  {
  define('PARIS_ORDERFILES_PROCESSED', '/mnt/SR/Insert_SR/processed');
  }

if ((!defined('PARIS_ORDERFILES_PATTERN')) || (strlen(PARIS_ORDERFILES_PATTERN) <= 0))
  {
  define('PARIS_ORDERFILES_PATTERN', '#[a-zA-Z0-9_\.]+VTO11I6[a-zA-Z0-9_\.]+$#');
  }

if ((!defined('PARIS_ORDERFILES_SEPARATOR')) || (strlen(PARIS_ORDERFILES_SEPARATOR) <= 0))
  {
  define('PARIS_ORDERFILES_SEPARATOR', ';');
  }

// RocheBrune Subscription / Redemption file constants

if ((!defined('RB_ORDERFILES_LOCALE')) || (strlen(RB_ORDERFILES_LOCALE) <= 0))
{
    define('RB_ORDERFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
}

if ((!defined('RB_ORDERFILES')) || (strlen(RB_ORDERFILES) <= 0))
{
    define('RB_ORDERFILES', '/mnt/PACK_VL');
}

if ((!defined('RB_ORDERFILES_PROCESSED')) || (strlen(RB_ORDERFILES_PROCESSED) <= 0))
{
    define('RB_ORDERFILES_PROCESSED', '/mnt/PACK_VL/processed');
}

if ((!defined('RB_ORDERFILES_PATTERN')) || (strlen(RB_ORDERFILES_PATTERN) <= 0))
{
    define('RB_ORDERFILES_PATTERN', '#[a-zA-Z0-9_\.]+VTO11I6[a-zA-Z0-9_\.]+$#');
}

if ((!defined('RB_ORDERFILES_SEPARATOR')) || (strlen(RB_ORDERFILES_SEPARATOR) <= 0))
{
    define('RB_ORDERFILES_SEPARATOR', ';');
}


// AltaRocca Subscription / Redemption file constants

if ((!defined('AR_ORDERFILES_LOCALE')) || (strlen(AR_ORDERFILES_LOCALE) <= 0))
{
    define('AR_ORDERFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
}

if ((!defined('AR_ORDERFILES')) || (strlen(AR_ORDERFILES) <= 0))
{
    define('AR_ORDERFILES', '/mnt/PACK_VL');
}

if ((!defined('AR_ORDERFILES_PROCESSED')) || (strlen(AR_ORDERFILES_PROCESSED) <= 0))
{
    define('AR_ORDERFILES_PROCESSED', '/mnt/PACK_VL/processed');
}

if ((!defined('AR_ORDERFILES_PATTERN')) || (strlen(AR_ORDERFILES_PATTERN) <= 0))
{
    define('AR_ORDERFILES_PATTERN', '#[a-zA-Z0-9_\.]+VTO11I6[a-zA-Z0-9_\.]+$#');
}

if ((!defined('AR_ORDERFILES_SEPARATOR')) || (strlen(AR_ORDERFILES_SEPARATOR) <= 0))
{
    define('AR_ORDERFILES_SEPARATOR', ';');
}


// Executions file constants

if ((!defined('EXECUTIONS_FILE_NAME')) || (strlen(EXECUTIONS_FILE_NAME) <= 0))
{
    define('EXECUTIONS_FILE_NAME', 'GTOAPRX1');
}

if ((!defined('EXECUTIONS_FILE_DIRECTORY')) || (strlen(EXECUTIONS_FILE_DIRECTORY) <= 0))
{
    define('EXECUTIONS_FILE_DIRECTORY', '/mnt/executions');
}

if ((!defined('EXECUTIONS_FILE_DELAY_SECONDS')) || (strlen(EXECUTIONS_FILE_DELAY_SECONDS) <= 0))
{
    define('EXECUTIONS_FILE_DELAY_SECONDS', 60);
}

if ((!defined('EXECUTIONS_FILES_ACKNOWLEDGEMENT')) || (strlen(EXECUTIONS_FILES_ACKNOWLEDGEMENT) <= 0))
{
    define('EXECUTIONS_FILES_ACKNOWLEDGEMENT', '/mnt/acknowledgements/bp2s');
}

if ((!defined('EXECUTIONS_FILES_ACKNOWLEDGEMENT_PROCESSED')) || (strlen(EXECUTIONS_FILES_ACKNOWLEDGEMENT_PROCESSED) <= 0))
{
    define('EXECUTIONS_FILES_ACKNOWLEDGEMENT_PROCESSED', '/mnt/acknowledgements/bp2s/processed');
}

if ((!defined('EXECUTIONS_FILES_ACKNOWLEDGEMENT_PATTERN')) || (strlen(EXECUTIONS_FILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
{
    define('EXECUTIONS_FILES_ACKNOWLEDGEMENT_PATTERN', '#^[a-zA-Z0-9_\.]+\.VTO5226W\.[a-zA-Z0-9_\.]+$#');
}

if ((!defined('EXECUTIONS_FILES_LOCALE')) || (strlen(EXECUTIONS_FILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
{
    define('EXECUTIONS_FILES_LOCALE', 'fr-FR');
}


// corporate events file constants


if ((!defined('EVENTSFILES_LOCALE')) || (strlen(EVENTSFILES_LOCALE) <= 0))
{
    define('EVENTSFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
}

if ((!defined('EVENTSFILES')) || (strlen(EVENTSFILES) <= 0))
{
    define('EVENTSFILES', '/mnt/events');
}

if ((!defined('EVENTSFILES_PROCESSED')) || (strlen(EVENTSFILES_PROCESSED) <= 0))
{
    define('EVENTSFILES_PROCESSED', '/mnt/events/processed');
}

if ((!defined('EVENTSFILES_PATTERN')) || (strlen(EVENTSFILES_PATTERN) <= 0))
{
    define('EVENTSFILES_PATTERN', '#[a-zA-Z0-9_\.]+VTO11I6[a-zA-Z0-9_\.]+$#');
}

if ((!defined('EVENTSFILES_SEPARATOR')) || (strlen(EVENTSFILES_SEPARATOR) <= 0))
{
    define('EVENTSFILES_SEPARATOR', ',');
}

// pack VL and Inventory files constants


if ((!defined('VLFILES')) || (strlen(VLFILES) <= 0))
{
    define('VLFILES', '/mnt/PACK_VL');
}

if ((!defined('VLFILES_PROCESSED')) || (strlen(VLFILES_PROCESSED) <= 0))
{
    define('VLFILES_PROCESSED', '/mnt/PACK_VL/processed');
}

if ((!defined('VLFILES_PATTERN')) || (strlen(VLFILES_PATTERN) <= 0))
{
    define('VLFILES_PATTERN', '/Rapport_validation/');
}

if ((!defined('INVENTORY_PATTERN')) || (strlen(INVENTORY_PATTERN) <= 0))
{
    define('INVENTORY_PATTERN', '/FA_INVE_ALL/');
}

// NeolinkConfirmations files constants


if ((!defined('NEOLINKCONFIRMATIONFILES')) || (strlen(NEOLINKCONFIRMATIONFILES) <= 0))
{
	define('NEOLINKCONFIRMATIONFILES', '/mnt/neolink');
}

if ((!defined('NEOLINKCONFIRMATIONFILES_PROCESSED')) || (strlen(NEOLINKCONFIRMATIONFILES_PROCESSED) <= 0))
{
	define('NEOLINKCONFIRMATIONFILES_PROCESSED', '/mnt/neolink/processed');
}


// AMF files constants

if ((!defined('AMF_FILE_DIRECTORY')) || (strlen(AMF_FILE_DIRECTORY) <= 0))
{
    define('AMF_FILE_DIRECTORY', '/mnt/amf');
}


?>
