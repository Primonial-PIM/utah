<?php

function dom_inventory_table($url_content, $fundcode, $id, $sqlsvr_id, $thisFundNavDate, $sql_DateEntered)
  {
  //$table="<table border='1' class=\"broom_table\"><thead>";

  $names = array();

  $AllExistingInventories = get_existing_inventory($fundcode, $thisFundNavDate);
  $ExistingInventoryItems = array();
  foreach ($AllExistingInventories AS $row)
  {
      // Key is converted to UCase, as it is later, because SQL Server is not case sensitive (In our configuration).

      $KeyName = strtoupper($row['neo_isin'] . "_" . $row['neo_quotecurrency']);

      $ExistingInventoryItems[$KeyName]=$row;

  }



//$names[] = array('Database field name', flag : Should not carry forward if true, flag : set as '' string, max string length);
// carry-over flag does not work with this code, it's only in here for consistency with other code.

  $names[] = array('neo_checkall', 0, 0, 50);
  $names[] = array('neo_instrumenttype', 0, 0, 50);
  $names[] = array('neo_sessionstatus', 0, 0, 50);
  $names[] = array('neo_isin', 0, 0, 50);
  $names[] = array('neo_description', 0, 0, 255);
  $names[] = array('neo_quantity', 0, 0, 50);
  $names[] = array('neo_type', 0, 0, 50);
  $names[] = array('neo_pricedate', 0, 0, 50);
  $names[] = array('neo_price', 0, 0, 50);
  $names[] = array('neo_priceflag', 0, 0, 50);
  $names[] = array('neo_quotecurrency', 0, 0, 5);
  $names[] = array('neo_marketvalue_qc', 0, 0, 50);
  $names[] = array('neo_unitprice_qc', 0, 0, 50);
  $names[] = array('neo_exchangerate', 0, 0, 50);
  $names[] = array('neo_referencecurrency', 0, 0, 50);
  $names[] = array('neo_marketvalue_rc', 0, 0, 50);
  $names[] = array('neo_accruedinterest_rc', 0, 0, 50);
  $names[] = array('neo_costprice_rc', 0, 0, 50);
  $names[] = array('neo_nonrealisedgainloss_rc', 0, 0, 50);
  $names[] = array('neo_percentnetassets', 0, 0, 50);
  $names[] = array('fundcode', 0, 0, 50);
  $names[] = array('neo_realisedgainloss_qc', 0, 0, 50);
  $names[] = array('neo_instrumentweight', 0, 0, 50);
  $names[] = array('neo_assettype', 0, 0, 50);
  $names[] = array('neo_subassettype', 0, 0, 50);
  $names[] = array('neo_subassettypedesc', 0, 1, 255);
  $names[] = array('neo_issuercode', 0, 0, 50);
  $names[] = array('neo_quotationplace', 0, 0, 50);
  $names[] = array('neo_quotationplacedesc', 0, 1, 255);
  $names[] = array('neo_cotationcountry', 0, 0, 50);
  $names[] = array('neo_maturitydate', 0, 0, 50);
  $names[] = array('neo_nominal', 0, 0, 50);
  $names[] = array('neo_underlyingassettype', 0, 0, 50);
  $names[] = array('neo_underlyingassettypedesc', 0, 1, 255);
  $names[] = array('neo_quotite', 0, 0, 50);
  $names[] = array('neo_strike', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('table')->item(0);

    // extract headings
    //foreach ($names as $name){
    //	$table.="<th>".$name."</th>";
    //}
    //$table.="</thead><tbody>";

    //extract body of table
    $odd = true;
    $count = 0;

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings

        if ($count > 0)
          {
          //if($odd){
          //	$table.="<tr class=\"odd datarow\">";
          //	$odd=false;
          //} else {
          //	$table.="<tr class=\"even datarow\">";
          //	$odd=true;
          //}
          //echo $row->nodeName . " = " . $row->nodeValue . "<br />";

          $cellcount = 0;
          $inventory = array();
          $info = $row->childNodes->length;
          $inventory['fundcode'] = '';

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              //echo $cell->nodeName . " = " . $cell->nodeValue . "<br />";
              //$table.="<td>".$cell->nodeValue.'</td>';

              if ($cellcount < $names_length)
                {
                $dataVal = ($names[$cellcount][2] ? '' : trim($cell->nodeValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $inventory[$names[$cellcount][0]] = $dataVal;
                }

              $cellcount++;
              }
            }

          if ($cellcount >= $names_length)
            {
            if (strlen($inventory['fundcode']) <= 0)
              {
              $inventory['fundcode'] = $fundcode;
              }

            $inventory['NAVDate'] = $thisFundNavDate;

            //$inventory['captureID'] = $id;
            //add_inventory($inventory);
            $inventory['captureID'] = $sqlsvr_id;
            add_inventory_sqlsvr($inventory, $names, $sql_DateEntered);

            $KeyName = strtoupper($inventory['neo_isin'] . "_" . $inventory['neo_quotecurrency']);
            if (array_key_exists ( $KeyName , $ExistingInventoryItems ))
                {
                    unset($ExistingInventoryItems[$KeyName]);
                }
            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

            return false;
            }

          //$table.="</tr>";
          }
        $count++;
        }

          // now remove Availabilities which do not seem to exist any more.
          foreach ($ExistingInventoryItems AS $row)
          {
              $inventory = array();

              foreach ($names as $name)
              {
                  if ($name[1] == 0)
                  {
                      $inventory[$name[0]] = '0';
                  }
              }

              $inventory['fundcode'] = $row['fundcode'];
              $inventory['neo_isin'] = $row['neo_isin'];
              $inventory['neo_quotecurrency'] = $row['neo_quotecurrency'];
              $inventory['NAVDate'] = $thisFundNavDate;
              $inventory['captureID'] = $sqlsvr_id;

              if ($row['neo_quantity_value'] != 0)
              {
                  add_inventory_sqlsvr($inventory, $names, $sql_DateEntered);
              }

          }

      }

    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; //$table."</tbody></table>";
  }

function add_inventory($params)
  {

  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "INSERT INTO rec_inventory (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    throw new Exception('inventory record could not be saved');
    //throw new Exception($query);
    }

  return $conn->insert_id;
  }

function add_inventory_sqlsvr($params, $names, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_inventory_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function get_existing_Inventory($fundcode, $thisFundNavDate)
{
    $results_array = array();

    try
    {
        $sqlsvr_conn = sqlserver_neocapture_connect();

        // Create a new stored prodecure
        $stmt = mssql_init('get_Inventory', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {
            if (substr(phpversion(), 0, 4) == '5.3.')
            {
                mssql_bind($stmt, '@NAVDate', &$thisFundNavDate, SQLVARCHAR, false, false, 50);
                mssql_bind($stmt, '@FundCode', &$fundcode, SQLVARCHAR, false, false, 50);
            } else
            {
                mssql_bind($stmt, '@NAVDate', $thisFundNavDate, SQLVARCHAR, false, false, 50);
                mssql_bind($stmt, '@FundCode', $fundcode, SQLVARCHAR, false, false, 50);
            }

            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
            }
            else if ($proc_result !== true)
            {
                if (mssql_num_rows($proc_result) > 0)
                {
                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[] = $row;
                    }
                }
            }

            // Free statement
            mssql_free_statement($stmt);
        }

    }
    catch (Exception $e)
    {
    }

    return $results_array;

}

?>
