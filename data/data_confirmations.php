<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');

function get_all_confirmations() {

	$conn = db_connect();

	$query = "SELECT *
	        FROM confirmations";

	if (!($result = $conn->query($query))) {
		throw new Exception('Could not find confirmations');
	}

	if ($result->num_rows==0) {
		throw new Exception('Could not find confirmations');
	}

	$results_array = array();
	// build an array of the relevant information
	for ($count=0; $row = $result->fetch_assoc(); $count++) {
		$results_array[$count] = $row;
	}
	return $results_array;
}

function get_confirmations($ID) {

	$conn = db_connect();

	$query="SELECT *
	        FROM confirmations
	        WHERE ID='".$conn->real_escape_string($ID)."'";

	if (!($result = $conn->query($query))) {
		throw new Exception('Could not find confirmations');
	}

	if ($result->num_rows==0) {
		throw new Exception('Could not find confirmations');
	}

	$results_array = array();
	// build an array of the relevant information record
	for ($count=0; $row = $result->fetch_assoc(); $count++) {
		$results_array = $row;
	}
	return $results_array;
}

function add_confirmations($params) {

    /*
	$conn = db_connect();
	
	// prepare insert query
	$name_array=array_keys($params);
	$value_array=array_values($params);
	$count=0;

	$query="INSERT INTO confirmations (";
	foreach($name_array as $name){
		if ($count>=0){
			if ($count>0)$query=$query." ,";
			$query=$query.$conn->real_escape_string($name);
		}
		$count++;
	}
	$query=$query.") VALUES(";
	$count=0;
	foreach($value_array as $value){
		if ($count>=0){
			if ($count>0)$query=$query." ,";
			$query=$query."'".$conn->real_escape_string($value)."'";
		}
		$count++;
	}
	$query=$query.")";
	
	// insert the new information record

	if (!$conn->query($query)) {
		throw new Exception('confirmations record could not be saved');
	}
*/
	return true;
    //return $query;
}

function add_confirmations_sqlsrv($params, $sql_DateEntered)
  {

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_confirmations_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_head', &$params['neo_head'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_bank', &$params['neo_bank'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_st', &$params['neo_st'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_status', &$params['neo_status'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_cat', &$params['neo_cat'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_t', &$params['neo_t'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_settlement', &$params['neo_settlement'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_clientreference', &$params['neo_clientreference'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_bankreference', &$params['neo_bankreference'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_quantity', &$params['neo_quantity'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_unit', &$params['neo_unit'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_isin', &$params['neo_isin'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_securitydescription', &$params['neo_securitydescription'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_settlementamount', &$params['neo_settlementamount'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settlementcurrency', &$params['neo_settlementcurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_price', &$params['neo_price'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_pricecurrency', &$params['neo_pricecurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_trade', &$params['neo_trade'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_securitiesaccount', &$params['neo_securitiesaccount'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_accountdescription', &$params['neo_accountdescription'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settagentbic', &$params['neo_settagentbic'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settagentdescr', &$params['neo_settagentdescr'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_ctrpbic', &$params['neo_ctrpbic'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_tradecounterpartdescr', &$params['neo_tradecounterpartdescr'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@instance', &$params['instance'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

// Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result===false)
        {
        $CaptureID = false;
        }
      else if ($proc_result!==true)
        {
        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];
        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    }
  catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function add_ucits_orders_sqlsrv($params, $sql_DateEntered)
  {

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_confirmations_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_head', &$params['neo_head'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_bank', &$params['neo_bank'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_st', &$params['neo_st'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_status', &$params['neo_status'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_cat', &$params['neo_cat'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_t', &$params['neo_t'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_settlement', &$params['neo_settlement'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_clientreference', &$params['neo_clientreference'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_bankreference', &$params['neo_bankreference'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_quantity', &$params['neo_quantity'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_unit', &$params['neo_unit'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_isin', &$params['neo_isin'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_securitydescription', &$params['neo_securitydescription'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_settlementamount', &$params['neo_settlementamount'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settlementcurrency', &$params['neo_settlementcurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_price', &$params['neo_price'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_pricecurrency', &$params['neo_pricecurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_trade', &$params['neo_trade'], SQLVARCHAR, false, false, 20);
    mssql_bind($stmt, '@neo_securitiesaccount', &$params['neo_securitiesaccount'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_accountdescription', &$params['neo_accountdescription'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settagentbic', &$params['neo_settagentbic'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_settagentdescr', &$params['neo_settagentdescr'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_ctrpbic', &$params['neo_ctrpbic'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_tradecounterpartdescr', &$params['neo_tradecounterpartdescr'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

// Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result===false)
        {
        $CaptureID = false;
        }
      else if ($proc_result!==true)
        {
        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];
        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    }
  catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function update_confirmations($params) {

	$conn = db_connect();

	// prepare insert query
	$name_array=array_keys($params);
	$value_array=array_values($params);
	$count=0;

	$query="UPDATE confirmations SET ";
	foreach($name_array as $name){
		if ($count>0)$query=$query." ,";
		$query=$query.$conn->real_escape_string($name)."='".$conn->real_escape_string($params[$name])."'";
		$count++;
	}
	$query=$query." WHERE ID=".$params['ID'];
	
	// replace the information record
	if (!$conn->query($query)) {
		throw new Exception('confirmations record could not be updated');
	}

	return true;
}

function delete_confirmations($ID) {

	$conn = db_connect();

	// prepare delete query

	$query="DELETE FROM confirmations ";
	$query=$query."WHERE ID='".$ID."'";

	// delete the information record
	if (!$conn->query($query)) {
		throw new Exception("Unable to delete confirmations record");
	}

	return true;
}

?>