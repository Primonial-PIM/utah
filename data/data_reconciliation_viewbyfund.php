<?php

function dom_viewbyfund_table($url_content, $id, $sqlsvr_id, $sql_DateEntered)
  {
  $returnArray = array();
  $viewbyfund = array(); // It's here for a reason !

  $names = array();
//$names[] = array('Database field name', flag : Should not carry forward if true);  // Cary forward is for sub fund information.

  $names[] = array('neo_expand', 1);
  $names[] = array('neo_tickall', 1);
  $names[] = array('neo_fundcode', 0);
  $names[] = array('neo_funddescription', 0);
  $names[] = array('neo_navdate', 0);
  $names[] = array('neo_st', 0);
  $names[] = array('neo_assets', 0);
  $names[] = array('neo_fundcurrency', 0);
  $names[] = array('neo_isin', 1);
  $names[] = array('neo_sharetype', 0);
  $names[] = array('neo_sharecurrency', 0);
  $names[] = array('neo_nav', 1);
  $names[] = array('neo_prevnav', 1);
  $names[] = array('neo_netassetsshare', 1);
  $names[] = array('neo_numberofshares', 1);
  $names[] = array('neo_prevnavdate', 0);
  $names[] = array('neo_refusalreason', 1);
  $names[] = array('neo_cutoff', 0);
  $names[] = array('neo_comments', 1);
  $names[] = array('neo_liquidity', 1);
  $names[] = array('neo_subshares', 1);
  $names[] = array('neo_redshares', 1);
  $names[] = array('neo_frequency', 0);
  $names[] = array('neo_navtype', 1);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('table')->item(0);

    //extract body of table
    //$odd = true;
    $count = 0;
    $viewbyfund = array();

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {

      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings
        if ($count > 0)
          {
          //if ($odd)
          //  {
          //  $table .= "<tr class=\"odd datarow\">";
          //  $odd = false;
          //  } else
          //  {
          //  $table .= "<tr class=\"even datarow\">";
          //  $odd = true;
          //  }

          //echo $row->nodeName . " = " . $row->nodeValue . "<br />";
          $cellcount = 0;

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              //echo $cell->nodeName . " = " . $cell->nodeValue . "<br />";
              //$table .= "<td>" . $cell->nodeValue . '</td>';

              $cell_value = $cell->nodeValue;

              if (($cellcount < $names_length) && (($names[$cellcount][1]) || (strlen($cell_value) > 0)))
                {
                $viewbyfund[$names[$cellcount][0]] = trim($cell->nodeValue);
                }

              $cellcount++;
              }
            } // foreach ($row->childNodes AS $cell)

          // Save fund information, if it has an ISIN or a NAV.

          if ((strlen($viewbyfund['neo_isin']) > 0) || (strlen($viewbyfund['neo_nav']) > 0))
            {
            if ($cellcount >= $names_length)
              {
              $returnArray[] = array("neo_fundcode" => $viewbyfund['neo_fundcode'], "neo_navdate" => $viewbyfund['neo_navdate'], "neo_funddescription" => $viewbyfund['neo_funddescription']);

              //$viewbyfund['captureID'] = $id;
              //add_viewbyfund($viewbyfund);
              $viewbyfund['captureID'] = $sqlsvr_id;
              add_viewbyfund_sqlsvr($viewbyfund, $sqlsvr_id, $sql_DateEntered);
              }
            else
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

              return false;
              }
            }

          //$table .= "</tr>";
          }
        $count++;
        }
      }
    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return $returnArray; // $table . "</tbody></table>";
  }

function add_viewbyfund($params)
  {
  /*
  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "INSERT INTO rec_viewbyfund (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    //throw new Exception('viewbyfund record could not be saved');
    throw new Exception($query);
    }

  return $conn->insert_id;
  */
  }

function add_viewbyfund_sqlsvr($params, $sqlsvr_id, $sql_DateEntered)
  {

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_viewbyfund_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_expand', &$params['neo_expand'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_tickall', &$params['neo_tickall'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_fundcode', &$params['neo_fundcode'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_funddescription', &$params['neo_funddescription'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_navdate', &$params['neo_navdate'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_prevnavdate', &$params['neo_prevnavdate'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_st', &$params['neo_st'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_assets', &$params['neo_assets'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_fundcurrency', &$params['neo_fundcurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_isin', &$params['neo_isin'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_sharetype', &$params['neo_sharetype'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_sharecurrency', &$params['neo_sharecurrency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@neo_nav', &$params['neo_nav'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_prevnav', &$params['neo_prevnav'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_netassetsshare', &$params['neo_netassetsshare'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_numberofshares', &$params['neo_numberofshares'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_liquidity', &$params['neo_liquidity'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_subshares', &$params['neo_subshares'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_redshares', &$params['neo_redshares'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_refusalreason', &$params['neo_refusalreason'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_comments', &$params['neo_comments'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_cutoff', &$params['neo_cutoff'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_frequency', &$params['neo_frequency'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_navtype', &$params['neo_navtype'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


// Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function get_viewbyfund_by_capture($id)
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM rec_viewbyfund WHERE captureID=" . $id . " ORDER BY viewbyfundID";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find viewbyfund');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find viewbyfund');
    }

  $results_array = array();
  // build an array of the relevant information
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array[$count] = $row;
    }
  return $results_array;
  }

?>
