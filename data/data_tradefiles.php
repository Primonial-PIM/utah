<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');

function add_capture_sqlserver($params, $sql_DateEntered)
  {
  try
    {

    $sqlsvr_conn = sqlserver_neocapture_connect();

    $CaptureID = 0;

// Create a new stored prodecure
    if ($sqlsvr_conn)
      {
      $stmt = mssql_init('dbo.adp_capture_InsertCommand', $sqlsvr_conn);

// Bind the field names
      mssql_bind($stmt, '@SetID', &$params['setID'], SQLINT4, false, false);
      mssql_bind($stmt, '@Result', &$params['result'], SQLVARCHAR, false, false, 255);
      mssql_bind($stmt, '@filename', &$params['filename'], SQLVARCHAR, false, false, 50);
      mssql_bind($stmt, '@priority', &$params['priority'], SQLINT4, false, false);
      mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

// Execute
      if ($stmt) $proc_result = mssql_execute($stmt);

      // Free statement
      mssql_free_statement($stmt);

      $row = mssql_fetch_assoc($proc_result);
      $CaptureID = $row['ID'];
      }  // if ($sqlsvr_conn)

    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function capturefailed($message, $thisFile)
  {
  
  }

function get_ExistingTradefiles_sqlsvr()
  {
  $returnArray = array();

  $filename = array();
  $priority = array();
  $result = array();

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      {
      $queryString = "SELECT [filename],[priority],[result] FROM dbo.fn_tblTradefilesCapture_SelectKD(NULL) WHERE [result] LIKE 'success%' ORDER BY [filename]";
      $queryResult = mssql_query($queryString, $link);

      //$row = mssql_fetch_array($result);

      if (mssql_num_rows($queryResult))
        {
        while ($row = mssql_fetch_assoc($queryResult))
          {
          $filename[] = $row['filename'];
          $priority[] = $row['priority'];
          $result[] = $row['result'];
          }
        }

      mssql_free_result($queryResult);

      }
    }
  catch (Exception $e)
    {
    }

  $returnArray = array(
    'filename'  => $filename,
    'priority'  => $priority,
    'result'  => $result
  );

  return $returnArray;
  }


function get_all_tradefiles()
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM tradefiles";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find tradefiles');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find tradefiles');
    }

  $results_array = array();
  // build an array of the relevant information
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array[$count] = $row;
    }
  return $results_array;
  }

function get_tradefiles($ID)
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM tradefiles
	        WHERE ID='" . $conn->real_escape_string($ID) . "'";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find tradefiles');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find tradefiles');
    }

  $results_array = array();
  // build an array of the relevant information record
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array = $row;
    }
  return $results_array;
  }

function add_tradefiles($params, $sql_DateEntered)
  {

  try
    {

    $conn = db_connect();

    // prepare insert query
    $name_array = array_keys($params);
    $value_array = array_values($params);
    $count = 0;

    $query = "INSERT INTO tradefiles (";
    foreach ($name_array as $name)
      {
      if ($count >= 0)
        {
        if ($count > 0) $query = $query . " ,";
        $query = $query . $conn->real_escape_string($name);
        }
      $count++;
      }
    $query = $query . ") VALUES(";
    $count = 0;
    foreach ($value_array as $value)
      {
      if ($count >= 0)
        {
        if ($count > 0) $query = $query . " ,";
        $query = $query . "'" . $conn->real_escape_string($value) . "'";
        }
      $count++;
      }
    $query = $query . ")";

    // insert the new information record
    if (!$conn->query($query))
      {
      throw new Exception('tradefiles record could not be saved');
      }

    } catch (Exception $e)
    {
    }


  return true;
  }

function add_tradefiles_sqlsrv($params, $sql_DateEntered)
  {

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_tradefiles_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    mssql_bind($stmt, '@tradefilename', &$params['tradefilename'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@tradefiledate', &$params['tradefiledate'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@status', &$params['status'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@office', &$params['office'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@sophis_refcon', &$params['sophis_refcon'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@instrumentcodetype', &$params['instrumentcodetype'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@instrumentcode', &$params['instrumentcode'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@transactiontype', &$params['transactiontype'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@units', &$params['units'], SQLFLT8, false, false);
    mssql_bind($stmt, '@value', &$params['value'], SQLFLT8, false, false);
    mssql_bind($stmt, '@currency', &$params['currency'], SQLVARCHAR, false, false, 5);
    mssql_bind($stmt, '@createoramend', &$params['createoramend'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@priority', &$params['priority'], SQLINT4, false, false, 50);
    mssql_bind($stmt, '@directory', &$params['directory'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@lux_account', &$params['lux_account'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@paris_securitiesaccount', &$params['paris_securitiesaccount'], SQLVARCHAR, false, false, 50);

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;

  }

function update_tradefiles($params)
  {

  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "UPDATE tradefiles SET ";
  foreach ($name_array as $name)
    {
    if ($count > 0) $query = $query . " ,";
    $query = $query . $conn->real_escape_string($name) . "='" . $conn->real_escape_string($params[$name]) . "'";
    $count++;
    }
  $query = $query . " WHERE ID=" . $params['ID'];

  // replace the information record
  if (!$conn->query($query))
    {
    throw new Exception('tradefiles record could not be updated');
    }

  return true;
  }

function delete_tradefiles($ID)
  {

  $conn = db_connect();

  // prepare delete query

  $query = "DELETE FROM tradefiles ";
  $query = $query . "WHERE ID='" . $ID . "'";

  // delete the information record
  if (!$conn->query($query))
    {
    throw new Exception("Unable to delete tradefiles record");
    }

  return true;
  }

 function get_tradeStringsParis_sqlsrv($SecondsSinceUpdate){
 	
 	// returns an array of associative arrays each comprising 'RN' and 'tradeString'

 $BrokerID = 1; // BNP Paris

 try
   {
   $sqlsvr_conn = renaissance_connect();
   //ini_set('display_errors', 1);

   // Create a new stored prodecure
   $stmt = mssql_init('web_getPendingTradeFileEntries', $sqlsvr_conn);

   mssql_bind($stmt, '@MinSecondsSinceUpdate', &$SecondsSinceUpdate, SQLINT4, false, false);
   mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);

   //
   $results_array = array();
   $count = 0;

   // Execute
   if ($stmt)
     {
     $proc_result = mssql_execute($stmt);

     if (($proc_result !== false) && ($proc_result !== true))
       {
       if (mssql_num_rows($proc_result) > 0)
         {

         while ($row = mssql_fetch_assoc($proc_result))
           {
           $results_array[$count] = $row;
           $count++;
           }

         }
       }

     // Free statement
     mssql_free_statement($stmt);

     }
   } catch (Exception $e)
   {
   }

   return $results_array;

 }

function get_RB_tradeStringsParis_sqlsrv($SecondsSinceUpdate){

    // returns an array of associative arrays each comprising 'RN' and 'tradeString'

    $BrokerID = 1; // BNP Paris

    try
    {
        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('web_getPendingTradeFileEntries', $sqlsvr_conn);

        mssql_bind($stmt, '@MinSecondsSinceUpdate', &$SecondsSinceUpdate, SQLINT4, false, false);
        mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);

        //
        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}

function set_tradeFilesDoneParis_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN' and 'filename'
    // do whatever magic is required to change the status of trades in Venice

    try
    {
        $RN = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_connect();
        //ini_set('display_errors', 1);


        //


        foreach ($results as $line)
        {
            $RN = $line['RN'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_setTradeFileWritten', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@RN', &$RN, SQLINT4, false, false);

                $proc_result = mssql_execute($stmt);

                if (mssql_num_rows($proc_result) > 0)
                {
                    $row = mssql_fetch_assoc($proc_result);

                    $results_array[$count++] = $row;
                }

                // Free statement
                mssql_free_statement($stmt);
            }

        } // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_RB_tradeFilesDoneParis_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN' and 'filename'
    // do whatever magic is required to change the status of trades in Venice

    try
    {
        $RN = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);


        //


        foreach ($results as $line)
        {
            $RN = $line['RN'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_setTradeFileWritten', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@RN', &$RN, SQLINT4, false, false);

                $proc_result = mssql_execute($stmt);

                if (mssql_num_rows($proc_result) > 0)
                {
                    $row = mssql_fetch_assoc($proc_result);

                    $results_array[$count++] = $row;
                }

                // Free statement
                mssql_free_statement($stmt);
            }

        } // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}
function set_execsFileDone_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN', 'filename' and TransactionParentID
    // do whatever magic is required to change the status of execs in Venice

    try
    {
        $transactPtID = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_connect();
        //ini_set('display_errors', 1);


        //


        foreach ($results as $line)
        {
            $UserID = 0;
            $Token  = 'Null';
            $transactionParentID = $line['TransactionParentID'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_tblTradeFileExecutionReporting_Delete', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {



                mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
                mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
                mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);

                mssql_execute($stmt);

                //if (mssql_num_rows($proc_result) > 0)
                //{
                //    $row = mssql_fetch_assoc($proc_result);
                //
                //    $results_array[$count++] = $row;
                //}

                // Free statement
                mssql_free_statement($stmt);
            }

        } // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_RB_execsFileDone_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN', 'filename' and TransactionParentID
    // do whatever magic is required to change the status of execs in Venice

    try
    {
        $transactPtID = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);


        //


        foreach ($results as $line)
        {
            $UserID = 0;
            $Token  = 'Null';
            $transactionParentID = $line['TransactionParentID'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_tblTradeFileExecutionReporting_Delete', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {



                mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
                mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
                mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);

                mssql_execute($stmt);

                //if (mssql_num_rows($proc_result) > 0)
                //{
                //    $row = mssql_fetch_assoc($proc_result);
                //
                //    $results_array[$count++] = $row;
                //}

                // Free statement
                mssql_free_statement($stmt);
            }

        } // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_tradeFilesAcknowledgementsBNPParis_sqlsrv($results){
// $results is an array of associative arrays each comprising 'transactionID', 'statusID' and 'text'

try
  {
  $RN = 0;
  $results_array = array();
  $count = 0;

  $sqlsvr_conn = renaissance_connect();
  //ini_set('display_errors', 1);

  foreach ($results as $line)
    {
    $BrokerID = 1; // BNP Paris
    $transactionID = $line['transactionID'];
    $statusID = $line['statusID'];
    $tradeStatus = substr($line['tradeStatus'], 0, 1000);
    $text = substr($line['text'], 0, 1000);

    // Create a new stored procedure

    $stmt = mssql_init('web_setTradeAcknowledgement', $sqlsvr_conn);

    // Execute
    if ($stmt)
      {
      mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);
      mssql_bind($stmt, '@TransactionParentID', &$transactionID, SQLINT4, false, false);
      mssql_bind($stmt, '@TransactionStatusID', &$statusID, SQLINT4, false, false);
      mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLVARCHAR, false, false);
      mssql_bind($stmt, '@AcknowledgementText', &$text, SQLVARCHAR, false, false);

      $proc_result = mssql_execute($stmt);

      // $proc_result = false ; Didn't work, $proc_result = true : Worked, but did not return anything.
      // In these cases mssql_num_rows($proc_result) fails because it expects a resource, not a boolean value.

      if (($proc_result === false) || ($proc_result === true))
        {
        $results_array = $proc_result;
        }
      else
        {
        if (mssql_num_rows($proc_result) > 0)
          {
          $row = mssql_fetch_assoc($proc_result);

          $results_array[$count++] = $row;
          }
        }

      // Free statement
      mssql_free_statement($stmt);
      }

    } // For each.

  } catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_tradeFilesAcknowledgementsBNPParis_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
  }

return $results_array;


}

function set_tradeFilesAcknowledgementsBNPParisRocheBrune_sqlsrv($results){
// $results is an array of associative arrays each comprising 'transactionID', 'statusID' and 'text'

    try
    {
        $RN = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);

        foreach ($results as $line)
        {
            $BrokerID = 1; // BNP Paris
            $transactionID = $line['transactionID'];
            $statusID = $line['statusID'];
            $tradeStatus = substr($line['tradeStatus'], 0, 1000);
            $text = substr($line['text'], 0, 1000);

            // Create a new stored procedure

            $stmt = mssql_init('web_setTradeAcknowledgement', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);
                mssql_bind($stmt, '@TransactionParentID', &$transactionID, SQLINT4, false, false);
                mssql_bind($stmt, '@TransactionStatusID', &$statusID, SQLINT4, false, false);
                mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@AcknowledgementText', &$text, SQLVARCHAR, false, false);

                $proc_result = mssql_execute($stmt);

                // $proc_result = false ; Didn't work, $proc_result = true : Worked, but did not return anything.
                // In these cases mssql_num_rows($proc_result) fails because it expects a resource, not a boolean value.

                if (($proc_result === false) || ($proc_result === true))
                {
                    $results_array = $proc_result;
                }
                else
                {
                    if (mssql_num_rows($proc_result) > 0)
                    {
                        $row = mssql_fetch_assoc($proc_result);

                        $results_array[$count++] = $row;
                    }
                }

                // Free statement
                mssql_free_statement($stmt);
            }

        } // For each.

    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_tradeFilesAcknowledgementsBNPParisRocheBrune_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $results_array;


}

function set_tradeFilesConfirmationBNPParis_sqlsrv($results)
  {

  try
    {
    $RN = 0;
    $results_array = array();
    $count = 0;

    $sqlsvr_conn = renaissance_connect();
    //ini_set('display_errors', 1);

    if ($sqlsvr_conn === false)
      {
      return false;
      }

    foreach ($results as $line)
      {

      $fileName = substr($line['fileName'], 0, 50);
      $BrokerID = 1; // BNP Paris
      $status = trim($line['status']);
      $cancelled = trim($line['cancelled']);
      $accountNumber = trim($line['accountNumber']);
      $transactionID = trim($line['transactionID']);
      $bankReference = trim($line['bankref']);
      $settlementDate = $line['settlementDate']; // string 'yyy-mm-dd'
      $navDate = $line['navDate']; // string 'yyy-mm-dd'
      $ISIN = trim($line['ISIN']);
      $category = trim($line['category']);
      $orderType = trim($line['orderType']);
      $quantity = $line['quantity']; // Numeric
      $quantityMetric = trim($line['quantityMetric']);
      $price = $line['price']; // Numeric
      $priceCurrency = trim($line['priceCurrency']);
      $grossCashLocal = $line['grossCashLocal']; // Numeric
      $grossCashLocalCurrency = trim($line['grossCashLocalCurrency']);
      $netCashSettlement = $line['netCashSettlement']; // Numeric
      $netCashSettlementCurrency = trim($line['netCashSettlementCurrency']);

      $text = substr($line['text'], 0, 2000);

      if (NEOCAPTURE_DEBUG_ECHO) echo '    MSSQL, Setting transaction Confirmation : ' . $transactionID . PHP_EOL;

      // Create a new stored procedure

      $stmt = false;
      $proc_result = false;
      $stmt = mssql_init('web_setTradeConfirmation', $sqlsvr_conn);

      // Execute
      if ($stmt)
        {
        if (substr(phpversion(), 0, 4) == '5.3.')
          {
          mssql_bind($stmt, '@FileName', &$fileName, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);
          mssql_bind($stmt, '@TransactionParentID', &$transactionID, SQLINT4, false, false);
          mssql_bind($stmt, '@Status', &$status, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Cancelled', &$cancelled, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@AccountNumber', &$accountNumber, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@BankReference', &$bankReference, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@SettlementDate', &$settlementDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@NavDate', &$navDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Category', &$category, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderType', &$orderType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Quantity', &$quantity, SQLFLT8, false, false);
          mssql_bind($stmt, '@QuantityMetric', &$quantityMetric, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Price', &$price, SQLFLT8, false, false);
          mssql_bind($stmt, '@PriceCurrency', &$priceCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@GrossCashLocal', &$grossCashLocal, SQLFLT8, false, false);
          mssql_bind($stmt, '@GrossCashLocalCurrency', &$grossCashLocalCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@NetCashSettlement', &$netCashSettlement, SQLFLT8, false, false);
          mssql_bind($stmt, '@NetCashSettlementCurrency', &$netCashSettlementCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ConfirmationText', &$text, SQLVARCHAR, false, false);
          }
        else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@FileName', $fileName, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@BrokerID', $BrokerID, SQLINT4, false, false);
          mssql_bind($stmt, '@TransactionParentID', $transactionID, SQLINT4, false, false);
          mssql_bind($stmt, '@Status', $status, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Cancelled', $cancelled, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@AccountNumber', $accountNumber, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@BankReference', $bankReference, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@SettlementDate', $settlementDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@NavDate', $navDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Category', $category, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderType', $orderType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Quantity', $quantity, SQLFLT8, false, false);
          mssql_bind($stmt, '@QuantityMetric', $quantityMetric, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Price', $price, SQLFLT8, false, false);
          mssql_bind($stmt, '@PriceCurrency', $priceCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@GrossCashLocal', $grossCashLocal, SQLFLT8, false, false);
          mssql_bind($stmt, '@GrossCashLocalCurrency', $grossCashLocalCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@NetCashSettlement', $netCashSettlement, SQLFLT8, false, false);
          mssql_bind($stmt, '@NetCashSettlementCurrency', $netCashSettlementCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ConfirmationText', $text, SQLVARCHAR, false, false);
          }

        $proc_result = mssql_execute($stmt);

        if ($proc_result === false)
          {
          // Return False if the statement fails to execute.
          $results_array = false;
          }
        else
          {
          if (mssql_num_rows($proc_result) > 0)
            {
            $row = mssql_fetch_assoc($proc_result);

            $results_array[$count++] = $row;
            }
          }

        // Free statement
        mssql_free_statement($stmt);
        }
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '      $stmt = mssql_init() = False.  Confirmation not saved.' . PHP_EOL;
        }

      } // For each.

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_tradeFilesConfirmationBNPParis_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

  return $results_array;

  }

function set_tradeFilesConfirmationBNPParisRocheBrune_sqlsrv($results)
{

    try
    {
        $RN = 0;
        $results_array = array();
        $count = 0;

        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);

        if ($sqlsvr_conn === false)
        {
            return false;
        }

        foreach ($results as $line)
        {

            $fileName = substr($line['fileName'], 0, 50);
            $BrokerID = 1; // BNP Paris
            $status = trim($line['status']);
            $cancelled = trim($line['cancelled']);
            $accountNumber = trim($line['accountNumber']);
            $transactionID = trim($line['transactionID']);
            $bankReference = trim($line['bankref']);
            $settlementDate = $line['settlementDate']; // string 'yyy-mm-dd'
            $navDate = $line['navDate']; // string 'yyy-mm-dd'
            $ISIN = trim($line['ISIN']);
            $category = trim($line['category']);
            $orderType = trim($line['orderType']);
            $quantity = $line['quantity']; // Numeric
            $quantityMetric = trim($line['quantityMetric']);
            $price = $line['price']; // Numeric
            $priceCurrency = trim($line['priceCurrency']);
            $grossCashLocal = $line['grossCashLocal']; // Numeric
            $grossCashLocalCurrency = trim($line['grossCashLocalCurrency']);
            $netCashSettlement = $line['netCashSettlement']; // Numeric
            $netCashSettlementCurrency = trim($line['netCashSettlementCurrency']);

            $text = substr($line['text'], 0, 2000);

            if (NEOCAPTURE_DEBUG_ECHO) echo '    MSSQL, Setting transaction Confirmation : ' . $transactionID . PHP_EOL;

            // Create a new stored procedure

            $stmt = false;
            $proc_result = false;
            $stmt = mssql_init('web_setTradeConfirmation', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                if (substr(phpversion(), 0, 4) == '5.3.')
                {
                    mssql_bind($stmt, '@FileName', &$fileName, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);
                    mssql_bind($stmt, '@TransactionParentID', &$transactionID, SQLINT4, false, false);
                    mssql_bind($stmt, '@Status', &$status, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Cancelled', &$cancelled, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@AccountNumber', &$accountNumber, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@BankReference', &$bankReference, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@SettlementDate', &$settlementDate, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@NavDate', &$navDate, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Category', &$category, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@OrderType', &$orderType, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Quantity', &$quantity, SQLFLT8, false, false);
                    mssql_bind($stmt, '@QuantityMetric', &$quantityMetric, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Price', &$price, SQLFLT8, false, false);
                    mssql_bind($stmt, '@PriceCurrency', &$priceCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@GrossCashLocal', &$grossCashLocal, SQLFLT8, false, false);
                    mssql_bind($stmt, '@GrossCashLocalCurrency', &$grossCashLocalCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@NetCashSettlement', &$netCashSettlement, SQLFLT8, false, false);
                    mssql_bind($stmt, '@NetCashSettlementCurrency', &$netCashSettlementCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@ConfirmationText', &$text, SQLVARCHAR, false, false);
                }
                else
                {
                    // Pass by reference deprecated in PHP 5.4

                    mssql_bind($stmt, '@FileName', $fileName, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@BrokerID', $BrokerID, SQLINT4, false, false);
                    mssql_bind($stmt, '@TransactionParentID', $transactionID, SQLINT4, false, false);
                    mssql_bind($stmt, '@Status', $status, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Cancelled', $cancelled, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@AccountNumber', $accountNumber, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@BankReference', $bankReference, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@SettlementDate', $settlementDate, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@NavDate', $navDate, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Category', $category, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@OrderType', $orderType, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Quantity', $quantity, SQLFLT8, false, false);
                    mssql_bind($stmt, '@QuantityMetric', $quantityMetric, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@Price', $price, SQLFLT8, false, false);
                    mssql_bind($stmt, '@PriceCurrency', $priceCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@GrossCashLocal', $grossCashLocal, SQLFLT8, false, false);
                    mssql_bind($stmt, '@GrossCashLocalCurrency', $grossCashLocalCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@NetCashSettlement', $netCashSettlement, SQLFLT8, false, false);
                    mssql_bind($stmt, '@NetCashSettlementCurrency', $netCashSettlementCurrency, SQLVARCHAR, false, false);
                    mssql_bind($stmt, '@ConfirmationText', $text, SQLVARCHAR, false, false);
                }

                $proc_result = mssql_execute($stmt);

                if ($proc_result === false)
                {
                    // Return False if the statement fails to execute.
                    $results_array = false;
                }
                else
                {
                    if (mssql_num_rows($proc_result) > 0)
                    {
                        $row = mssql_fetch_assoc($proc_result);

                        $results_array[$count++] = $row;
                    }
                }

                // Free statement
                mssql_free_statement($stmt);
            }
            else
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '      $stmt = mssql_init() = False.  Confirmation not saved.' . PHP_EOL;
            }

        } // For each.

    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_tradeFilesConfirmationBNPParis_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $results_array;

}

function set_SubscriptionBNPParis_sqlsrv($results)
  {
  /* Function to save Subscription / Redemption orders.
   *
   *
   * Note, a return value === false will prevent the Order file being moved. Do not do this if any orders are saved
   * as this will probably result in them being entered multiple times.
   */
  try
    {
    $results_array = array();
    $count = 0;

    $lineCount = 0;
    $autoUpdateValue = 0;

    $sqlsvr_conn = renaissance_connect();
    ini_set('display_errors', 1);

    if ($sqlsvr_conn === false)
      {
      return false;
      }
    else
      {
      mssql_close($sqlsvr_conn);
      }

    foreach ($results as $line)
      {
      // Set $autoUpdateValue for final Order line.
      // The idea is that an update message will only be sent after the final order is entered.

      $lineCount++;
      if ($lineCount >= count($results)) $autoUpdateValue = 1;

      $results_array[$count++] = set_SingleSubscriptionBNPParis_sqlsrv($line, $autoUpdateValue);

      } // For each.

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SubscriptionBNPParis_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

  return $results_array;

  }

function set_SubscriptionRocheBrune_sqlsrv($results)
{
    /* Function to save Subscription / Redemption orders.
     *
     *
     * Note, a return value === false will prevent the Order file being moved. Do not do this if any orders are saved
     * as this will probably result in them being entered multiple times.
     */
    try
    {
        $results_array = array();
        $count = 0;

        $lineCount = 0;
        $autoUpdateValue = 0;

        $sqlsvr_conn = renaissance_RB_connect();
        ini_set('display_errors', 1);

        if ($sqlsvr_conn === false)
        {
            return false;
        }
        else
        {
            mssql_close($sqlsvr_conn);
        }

        foreach ($results as $line)
        {
            // Set $autoUpdateValue for final Order line.
            // The idea is that an update message will only be sent after the final order is entered.

            $lineCount++;
            if ($lineCount >= count($results)) $autoUpdateValue = 1;

            $results_array[$count++] = set_SingleSubscriptionRocheBrune_sqlsrv($line, $autoUpdateValue);

        } // For each.

    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SubscriptionRocheBrune_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $results_array;

}

function set_SubscriptionAltaRocca_sqlsrv($results)
{
    /* Function to save Subscription / Redemption orders.
     *
     *
     * Note, a return value === false will prevent the Order file being moved. Do not do this if any orders are saved
     * as this will probably result in them being entered multiple times.
     */
    try
    {
        $results_array = array();
        $count = 0;

        $lineCount = 0;
        $autoUpdateValue = 0;

        $sqlsvr_conn = renaissance_AR_connect();
        ini_set('display_errors', 1);

        if ($sqlsvr_conn === false)
        {
            return false;
        }
        else
        {
            mssql_close($sqlsvr_conn);
        }

        foreach ($results as $line)
        {
            // Set $autoUpdateValue for final Order line.
            // The idea is that an update message will only be sent after the final order is entered.

            $lineCount++;
            if ($lineCount >= count($results)) $autoUpdateValue = 1;

            $results_array[$count++] = set_SingleSubscriptionAltaRocca_sqlsrv($line, $autoUpdateValue);

        } // For each.

    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SubscriptionAltaRocca_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $results_array;

}

function set_SingleSubscriptionBNPParis_sqlsrv($line, $autoUpdateValue)
  {
  $rVal = true;

   try
    {
    $sqlsvr_conn = renaissance_connect();

    // ini_set('display_errors', 1);

    if ($sqlsvr_conn === false)
      {
      return false;
      }

      // Initialise parameter variables.

      $fileName = substr($line['fileName'], 0, 50);
      $text = substr($line['text'], 0, 2000);

      $source = 'BNP';
      $ISIN =  $line['ISIN'];
      $Account = $line['account'];
      $orderDate = $line['orderDate']; //Date // string 'yyy-mm-dd'
      $settlementDate = $line['settlementDate']; //Date // string 'yyy-mm-dd'
      $unitCurrency = $line['unitCurrency'];
      $priceType = $line['priceType'];
      $priceDate = $line['priceDate']; //Date // string 'yyy-mm-dd'
      $price = $line['price']; // Numeric
      $priceCurrency = $line['priceCurrency'];
      $lineType = $line['lineType']; // 'Ordres', 'Recap', 'Net'
      $originatorID = $line['originatorID'];
      $originatorType = $line['originatorType'];
      $clientID = $line['clientID'];
      $clientType = $line['clientType'];
      $orderType = $line['orderType']; // 'SUBS', 'REDM'
      $quantity = $line['quantity']; // Numeric
      $amount = $line['amount']; // Numeric
      $settlementAmount = $line['settlement']; // Numeric
      $tradeStatus = 21; /* 24 : Checked EOD, 21 : Pending MO, 22 Pending EOD */

      if (NEOCAPTURE_DEBUG_ECHO) echo '    MSSQL, Adding Subscription / Redemption.' . PHP_EOL;

      // Create a new stored procedure

      $stmt = mssql_init('web_addSubscriptionRedemption', $sqlsvr_conn);

      // Execute
      if ($stmt)
        {
        if (substr(phpversion(), 0, 4) == '5.3.')
          {
          mssql_bind($stmt, '@Source', &$source, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@FileName', &$fileName, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Account', &$Account, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderDate', &$orderDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@SettlementDate', &$settlementDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@UnitCurrency', &$unitCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceType', &$priceType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceDate', &$priceDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Price', &$price, SQLFLT8, false, false);
          mssql_bind($stmt, '@PriceCurrency', &$priceCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@LineType', &$lineType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OriginatorID', &$originatorID, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OriginatorType', &$originatorType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ClientID', &$clientID, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ClientType', &$clientType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderType', &$orderType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Quantity', &$quantity, SQLFLT8, false, false);
          mssql_bind($stmt, '@Amount', &$amount, SQLFLT8, false, false);
          mssql_bind($stmt, '@SettlementAmount', &$settlementAmount, SQLFLT8, false, false);
          mssql_bind($stmt, '@OrderText', &$text, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLINT4, false, false);
          mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
          }
        else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@Source', $source, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@FileName', $fileName, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Account', $Account, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderDate', $orderDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@SettlementDate', $settlementDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@UnitCurrency', $unitCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceType', $priceType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceDate', $priceDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Price', $price, SQLFLT8, false, false);
          mssql_bind($stmt, '@PriceCurrency', $priceCurrency, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@LineType', $lineType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OriginatorID', $originatorID, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OriginatorType', $originatorType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ClientID', $clientID, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ClientType', $clientType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@OrderType', $orderType, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Quantity', $quantity, SQLFLT8, false, false);
          mssql_bind($stmt, '@Amount', $amount, SQLFLT8, false, false);
          mssql_bind($stmt, '@SettlementAmount', $settlementAmount, SQLFLT8, false, false);
          mssql_bind($stmt, '@OrderText', $text, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@TradeStatus', $tradeStatus, SQLINT4, false, false);
          mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
          }

        $proc_result = mssql_execute($stmt);

        if ($proc_result === false)
          {
          // Return False if the statement fails to execute.
          $rVal =  false;
          }
        else
          {
          if (mssql_num_rows($proc_result) > 0)
            {
            $rVal = mssql_fetch_assoc($proc_result);
            }
          }

        // Free statement
        mssql_free_statement($stmt);
        }
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '      $stmt = mssql_init() = False.  Order not saved.' . PHP_EOL;
        }



    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SubscriptionBNPParis_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

  return $rVal;

  }

function set_SingleSubscriptionRocheBrune_sqlsrv($line, $autoUpdateValue)
{
    $rVal = true;

    try
    {
        $sqlsvr_conn = renaissance_RB_connect();

        // ini_set('display_errors', 1);

        if ($sqlsvr_conn === false)
        {
            return false;
        }

        // Initialise parameter variables.

        $fileName = substr($line['fileName'], 0, 50);
        $text = substr($line['text'], 0, 2000);

        $source = 'BNP';
        $ISIN =  $line['ISIN'];
        $Account = $line['account'];
        $orderDate = $line['orderDate']; //Date // string 'yyy-mm-dd'
        $settlementDate = $line['settlementDate']; //Date // string 'yyy-mm-dd'
        $unitCurrency = $line['unitCurrency'];
        $priceType = $line['priceType'];
        $priceDate = $line['priceDate']; //Date // string 'yyy-mm-dd'
        $price = $line['price']; // Numeric
        $priceCurrency = $line['priceCurrency'];
        $lineType = $line['lineType']; // 'Ordres', 'Recap', 'Net'
        $originatorID = $line['originatorID'];
        $originatorType = $line['originatorType'];
        $clientID = $line['clientID'];
        $clientType = $line['clientType'];
        $orderType = $line['orderType']; // 'SUBS', 'REDM'
        $quantity = $line['quantity']; // Numeric
        $amount = $line['amount']; // Numeric
        $settlementAmount = $line['settlement']; // Numeric
        $tradeStatus = 21; /* 24 : Checked EOD, 21 : Pending MO, 22 Pending EOD */

        if (NEOCAPTURE_DEBUG_ECHO) echo '    MSSQL, Adding Subscription / Redemption.' . PHP_EOL;

        // Create a new stored procedure

        $stmt = mssql_init('web_addSubscriptionRedemption', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {
            if (substr(phpversion(), 0, 4) == '5.3.')
            {
                mssql_bind($stmt, '@Source', &$source, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@FileName', &$fileName, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Account', &$Account, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderDate', &$orderDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@SettlementDate', &$settlementDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@UnitCurrency', &$unitCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceType', &$priceType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceDate', &$priceDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Price', &$price, SQLFLT8, false, false);
                mssql_bind($stmt, '@PriceCurrency', &$priceCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@LineType', &$lineType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorID', &$originatorID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorType', &$originatorType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientID', &$clientID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientType', &$clientType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderType', &$orderType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Quantity', &$quantity, SQLFLT8, false, false);
                mssql_bind($stmt, '@Amount', &$amount, SQLFLT8, false, false);
                mssql_bind($stmt, '@SettlementAmount', &$settlementAmount, SQLFLT8, false, false);
                mssql_bind($stmt, '@OrderText', &$text, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLINT4, false, false);
                mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
            }
            else
            {
                // Pass by reference deprecated in PHP 5.4

                mssql_bind($stmt, '@Source', $source, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@FileName', $fileName, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Account', $Account, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderDate', $orderDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@SettlementDate', $settlementDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@UnitCurrency', $unitCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceType', $priceType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceDate', $priceDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Price', $price, SQLFLT8, false, false);
                mssql_bind($stmt, '@PriceCurrency', $priceCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@LineType', $lineType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorID', $originatorID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorType', $originatorType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientID', $clientID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientType', $clientType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderType', $orderType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Quantity', $quantity, SQLFLT8, false, false);
                mssql_bind($stmt, '@Amount', $amount, SQLFLT8, false, false);
                mssql_bind($stmt, '@SettlementAmount', $settlementAmount, SQLFLT8, false, false);
                mssql_bind($stmt, '@OrderText', $text, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@TradeStatus', $tradeStatus, SQLINT4, false, false);
                mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
            }

            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                // Return False if the statement fails to execute.
                $rVal =  false;
            }
            else
            {
                if (mssql_num_rows($proc_result) > 0)
                {
                    $rVal = mssql_fetch_assoc($proc_result);
                }
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            if (NEOCAPTURE_DEBUG_ECHO) echo '      $stmt = mssql_init() = False.  Order not saved.' . PHP_EOL;
        }



    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SingleSubscriptionRocheBrune_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $rVal;

}

function set_SingleSubscriptionAltaRocca_sqlsrv($line, $autoUpdateValue)
{
    $rVal = true;

    try
    {
        $sqlsvr_conn = renaissance_AR_connect();

        // ini_set('display_errors', 1);

        if ($sqlsvr_conn === false)
        {
            return false;
        }

        // Initialise parameter variables.

        $fileName = substr($line['fileName'], 0, 50);
        $text = substr($line['text'], 0, 2000);

        $source = 'BNP';
        $ISIN =  $line['ISIN'];
        $Account = $line['account'];
        $orderDate = $line['orderDate']; //Date // string 'yyy-mm-dd'
        $settlementDate = $line['settlementDate']; //Date // string 'yyy-mm-dd'
        $unitCurrency = $line['unitCurrency'];
        $priceType = $line['priceType'];
        $priceDate = $line['priceDate']; //Date // string 'yyy-mm-dd'
        $price = $line['price']; // Numeric
        $priceCurrency = $line['priceCurrency'];
        $lineType = $line['lineType']; // 'Ordres', 'Recap', 'Net'
        $originatorID = $line['originatorID'];
        $originatorType = $line['originatorType'];
        $clientID = $line['clientID'];
        $clientType = $line['clientType'];
        $orderType = $line['orderType']; // 'SUBS', 'REDM'
        $quantity = $line['quantity']; // Numeric
        $amount = $line['amount']; // Numeric
        $settlementAmount = $line['settlement']; // Numeric
        $tradeStatus = 21; /* 24 : Checked EOD, 21 : Pending MO, 22 Pending EOD */

        if (NEOCAPTURE_DEBUG_ECHO) echo '    MSSQL, Adding Subscription / Redemption.' . PHP_EOL;

        // Create a new stored procedure

        $stmt = mssql_init('web_addSubscriptionRedemption', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {
            if (substr(phpversion(), 0, 4) == '5.3.')
            {
                mssql_bind($stmt, '@Source', &$source, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@FileName', &$fileName, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Account', &$Account, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderDate', &$orderDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@SettlementDate', &$settlementDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@UnitCurrency', &$unitCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceType', &$priceType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceDate', &$priceDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Price', &$price, SQLFLT8, false, false);
                mssql_bind($stmt, '@PriceCurrency', &$priceCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@LineType', &$lineType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorID', &$originatorID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorType', &$originatorType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientID', &$clientID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientType', &$clientType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderType', &$orderType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Quantity', &$quantity, SQLFLT8, false, false);
                mssql_bind($stmt, '@Amount', &$amount, SQLFLT8, false, false);
                mssql_bind($stmt, '@SettlementAmount', &$settlementAmount, SQLFLT8, false, false);
                mssql_bind($stmt, '@OrderText', &$text, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLINT4, false, false);
                mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
            }
            else
            {
                // Pass by reference deprecated in PHP 5.4

                mssql_bind($stmt, '@Source', $source, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@FileName', $fileName, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Account', $Account, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderDate', $orderDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@SettlementDate', $settlementDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@UnitCurrency', $unitCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceType', $priceType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@PriceDate', $priceDate, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Price', $price, SQLFLT8, false, false);
                mssql_bind($stmt, '@PriceCurrency', $priceCurrency, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@LineType', $lineType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorID', $originatorID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OriginatorType', $originatorType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientID', $clientID, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@ClientType', $clientType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@OrderType', $orderType, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@Quantity', $quantity, SQLFLT8, false, false);
                mssql_bind($stmt, '@Amount', $amount, SQLFLT8, false, false);
                mssql_bind($stmt, '@SettlementAmount', $settlementAmount, SQLFLT8, false, false);
                mssql_bind($stmt, '@OrderText', $text, SQLVARCHAR, false, false);
                mssql_bind($stmt, '@TradeStatus', $tradeStatus, SQLINT4, false, false);
                mssql_bind($stmt, '@UpdateAutoUpdate', &$autoUpdateValue, SQLINT4, false, false);
            }

            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                // Return False if the statement fails to execute.
                $rVal =  false;
            }
            else
            {
                if (mssql_num_rows($proc_result) > 0)
                {
                    $rVal = mssql_fetch_assoc($proc_result);
                }
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            if (NEOCAPTURE_DEBUG_ECHO) echo '      $stmt = mssql_init() = False.  Order not saved.' . PHP_EOL;
        }



    } catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_SingleSubscriptionAltaRocca_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

    return $rVal;

}

/* BNP Luxembourg */

function get_tradeStringsBNPLuxembourg_sqlsrv($SecondsSinceUpdate){

// returns an array of associative arrays each comprising 'RN' and 'tradeString'

$BrokerID = 2; // BNP Luxembourg

try
  {
  $sqlsvr_conn = renaissance_connect();
  //ini_set('display_errors', 1);

  // Create a new stored prodecure
  $stmt = mssql_init('web_getPendingTradeFileEntries', $sqlsvr_conn);

  mssql_bind($stmt, '@MinSecondsSinceUpdate', &$SecondsSinceUpdate, SQLINT4, false, false);
  mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);

  //
  $results_array = array();
  $count = 0;

  // Execute
  if ($stmt)
    {
    $proc_result = mssql_execute($stmt);

    if (($proc_result !== false) && ($proc_result !== true))
      {
      if (mssql_num_rows($proc_result) > 0)
        {

        while ($row = mssql_fetch_assoc($proc_result))
          {
          $results_array[$count] = $row;
          $count++;
          }

        }
      }

    // Free statement
    mssql_free_statement($stmt);

    }
  } catch (Exception $e)
  {
  }

return $results_array;

}

function set_tradeFilesAcknowledgementsBNPLuxembourg_sqlsrv($results){
// $results is an array of associative arrays each comprising 'transactionID', 'statusID' and 'text'

try
  {
  $RN = 0;
  $results_array = array();
  $count = 0;

  //ini_set('mssql.charset', 'UTF-8');
  $sqlsvr_conn = renaissance_connect();
  //ini_set('display_errors', 1);

  foreach ($results as $line)
    {
    $BrokerID = 2; // BNP Luxembourg
    $transactionID = $line['transactionID'];
    $statusID = $line['statusID'];
    $tradeStatus = substr(trim($line['tradeStatus']), 0, 1000);
    $text = substr(trim($line['text']), 0, 1000);

    // Create a new stored procedure

    $stmt = mssql_init('web_setTradeAcknowledgement', $sqlsvr_conn);

    // Execute
    if ($stmt)
      {
      mssql_bind($stmt, '@BrokerID', &$BrokerID, SQLINT4, false, false);
      mssql_bind($stmt, '@TransactionParentID', &$transactionID, SQLINT4, false, false);
      mssql_bind($stmt, '@TransactionStatusID', &$statusID, SQLINT4, false, false);
      mssql_bind($stmt, '@TradeStatus', &$tradeStatus, SQLVARCHAR, false, false);
      mssql_bind($stmt, '@AcknowledgementText', &$text, SQLVARCHAR, false, false);

      $proc_result = mssql_execute($stmt);

      // $proc_result = false ; Didn't work, $proc_result = true : Worked, but did not return anything.
      // In these cases mssql_num_rows($proc_result) fails because it expects a resource, not a boolean value.

      if (($proc_result === false) || ($proc_result === true))
        {
        $results_array[$count++] = $proc_result;
        }
      else
        {
        if (mssql_num_rows($proc_result) > 0)
          {
          $row = mssql_fetch_assoc($proc_result);

          $results_array[$count++] = $row;
          }
        }

      // Free statement
      mssql_free_statement($stmt);
      }

    } // For each.

  } catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error in set_tradeFilesAcknowledgementsBNPLuxembourg_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
  }

return $results_array;


}



function get_Executions_sqlsrv($SecondsSinceUpdate){

    try
    {
        $sqlsvr_conn = renaissance_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('web_tblTradeFileExecutionReporting', $sqlsvr_conn);
        $UserID = 0;
        $Token  = 'Null';
        $knowledgeDate = '1900-01-01';

        mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate , SQLVARCHAR, false, false, 20);


        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}

function get_RB_Executions_sqlsrv($SecondsSinceUpdate){

    try
    {
        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('web_tblTradeFileExecutionReporting', $sqlsvr_conn);
        $UserID = 0;
        $Token  = 'Null';
        $knowledgeDate = '1900-01-01';

        mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate , SQLVARCHAR, false, false, 20);


        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}
?>