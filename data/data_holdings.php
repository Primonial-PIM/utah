<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');

function get_all_holdings()
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM holdings";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find holdings');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find holdings');
    }

  $results_array = array();
  // build an array of the relevant information
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array[$count] = $row;
    }
  return $results_array;
  }

function get_holdings($ID)
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM holdings
	        WHERE ID='" . $conn->real_escape_string($ID) . "'";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find holdings');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find holdings');
    }

  $results_array = array();
  // build an array of the relevant information record
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array = $row;
    }
  return $results_array;
  }

function add_holdings($params)
  {
/*
  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "INSERT INTO holdings (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    throw new Exception('Holdings record could not be saved');
    }
*/
  return true;
  }

function get_ExistingHoldings_sqlsvr($sqlsvr_id, $todaysDate)
  {
  $returnArray = array();
  $queryID = $sqlsvr_id -1;

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      {
      $queryString = "SELECT neo_securitiesaccount, neo_isin, neo_position, neo_security FROM dbo.fn_tblHoldings_SelectKD(NULL, " . $queryID . ", NULL) WHERE (neo_quantity_value <> 0) AND (CONVERT(date, DateEntered) = CONVERT(date, '" . $todaysDate . "', 121))";
      $result = mssql_query($queryString, $link);
      //$row = mssql_fetch_array($result);

      if (mssql_num_rows($result))
        {
        while ($row = mssql_fetch_array($result))
          {
            $returnArray[] = $row[0] . '|' . $row[1] . '|' . $row[2] . '|' . $row[3];
          }

        asort ($returnArray);
        }

      mssql_free_result($result);

      }
    }
  catch (Exception $e)
    {
    }

  return $returnArray;
  }

function add_holdings_sqlsrv($params, $sql_DateEntered)
  {

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_holdings_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_head', &$params['neo_head'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_a', &$params['neo_a'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_r', &$params['neo_r'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_ac', &$params['neo_ac'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_isin', &$params['neo_isin'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_security', &$params['neo_security'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_position', &$params['neo_position'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_quantity', &$params['neo_quantity'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_unit', &$params['neo_unit'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_value', &$params['neo_value'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_currency', &$params['neo_currency'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_depositary', &$params['neo_depositary'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_depositaryname', &$params['neo_depositaryname'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_bank', &$params['neo_bank'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_securitiesaccount', &$params['neo_securitiesaccount'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_securitiesaccountdescription', &$params['neo_securitiesaccountdescription'], SQLVARCHAR, false, false, 255);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

// Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result===false)
        {
        $CaptureID = false;
        }
      else if ($proc_result!==true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        // mssql_free_statement($proc_result);
        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    }
  catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function update_holdings($params)
  {

  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "UPDATE holdings SET ";
  foreach ($name_array as $name)
    {
    if ($count > 0) $query = $query . " ,";
    $query = $query . $conn->real_escape_string($name) . "='" . $conn->real_escape_string($params[$name]) . "'";
    $count++;
    }
  $query = $query . " WHERE ID=" . $params['ID'];

  // replace the information record
  if (!$conn->query($query))
    {
    throw new Exception('Holdings record could not be updated');
    }

  return true;
  }

function delete_holdings($ID)
  {

  $conn = db_connect();

  // prepare delete query

  $query = "DELETE FROM holdings ";
  $query = $query . "WHERE ID='" . $ID . "'";

  // delete the information record
  if (!$conn->query($query))
    {
    throw new Exception("Unable to delete holdings record");
    }

  return true;
  }

?>