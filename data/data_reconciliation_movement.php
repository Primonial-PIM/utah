<?php

function dom_movement_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered)
  {

  $names = array();

  $names[] = array('neo_clickall', 0, 0, 50);
  $names[] = array('neo_clientreference', 0, 0, 50);
  $names[] = array('neo_movementtype', 0, 0, 50);
  $names[] = array('neo_cancellation', 0, 0, 50);
  $names[] = array('neo_securitycode', 0, 0, 50);
  $names[] = array('neo_quantity', 0, 0, 50);
  $names[] = array('neo_quantityflag', 0, 0, 50);
  $names[] = array('neo_price', 0, 0, 50);
  $names[] = array('neo_priceflag', 0, 0, 50);
  $names[] = array('neo_quotecurrency', 0, 0, 5);
  $names[] = array('neo_netamount_pc', 0, 0, 50);
  $names[] = array('neo_paymentcurrency', 0, 0, 5);
  $names[] = array('neo_fees_rc', 0, 0, 50);
  $names[] = array('neo_costprice_rc', 0, 0, 50);
  $names[] = array('neo_unrealisedgainloss_fundcurrency', 0, 0, 50);
  $names[] = array('neo_referencecurrency', 0, 0, 5);
  $names[] = array('fundcode', 0, 0, 50);
  $names[] = array('neo_tradedate', 0, 0, 50);
  $names[] = array('neo_valuedate', 0, 0, 50);
  $names[] = array('neo_managementdate', 0, 0, 50);
  $names[] = array('neo_newquantity', 0, 0, 50);
  $names[] = array('neo_linecount', 0, 0, 50); // Must be at the end

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('table')->item(0);

    //extract body of table

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      $count = 0;
      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings

        if ($count > 0)
          {

          $cellcount = 0;
          $movement = array();
          $movement['fundcode'] = '';

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {

              if ($cellcount < $names_length)
                {
                $dataVal = ($names[$cellcount][2] ? '' : trim($cell->nodeValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $movement[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          if ($cellcount > 1)
            {

            if ($cellcount >= ($names_length-1)) /* minus 1 to allow for linecount  */
              {

              $movement['neo_linecount'] = (string)$count;

              //if data in the table then write to database
              if (strlen($movement['fundcode']) <= 0)
                {
                $movement['fundcode'] = $fundcode;
                }

              $movement['captureID'] = $id;
              //add_movement($movement);
              $movement['captureID'] = $sqlsvr_id;
              add_movement_sqlsvr($movement, $names, $sql_DateEntered);


              }
            else
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

              return false;
              }
            }
          }
        $count++;
        }
      }

    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; //$table."</tbody></table>";
  }

function add_movement($params)
  {

  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "INSERT INTO rec_movement (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    throw new Exception('movement record could not be saved');
    //throw new Exception($query);
    }

  return $conn->insert_id;
  }

function add_movement_sqlsvr($params, $names, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_movement_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        mssql_free_statement($proc_result);
        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

?>
