<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

/**
 * .
 *

 * BNP Contract note specification given in document '@PBL_REP_CNOT_ALL001 (2).doc', version 9/2/2013.

// Note the New DateTime() constructor requires that the timezone be set in the php.ini file.
// If the file is not being written, look at this.

// Note also, this file uses the PHP 'intl.so' extension. Not included by default on Ubuntu (apparently).
*/
require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'checkConfirmParisRocheBrune, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');

// Get trade confirmations from pending directory

$pendingDirectoryName = VLFILES;
$processedDirectoryName = VLFILES_PROCESSED;
$filePattern = PARIS_CONFIRMATIONFILES_RB_PATTERN;
$fileSeparator = PARIS_CONFIRMATIONFILES_SEPARATOR;

$acknowledgementLine = array();

// intl Formatters.

$Formatter_Decimal = numfmt_create( PARIS_CONFIRMATIONFILES_LOCALE, NumberFormatter::DECIMAL );
$Formatter_Date = datefmt_create ( PARIS_CONFIRMATIONFILES_LOCALE, IntlDateFormatter::SHORT , IntlDateFormatter::NONE, 'GMT'); /* Use GMT Timezone to avoid issues in parsing dates. */

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;

$directoryFiles = scandir($pendingDirectoryName);

try
  {
  if ($directoryFiles !== false)
    {
    foreach ($directoryFiles as $thisFile)
      {
      // is this file is a directory, especially . and ..

      if (is_dir($pendingDirectoryName . '/' . $thisFile))
        {
        continue;
        }

      if (preg_match($filePattern, $thisFile))
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

        /* Log this file */


        /* Process this file*/

        $results = array();
        $filecontents = file_get_contents($pendingDirectoryName . '/' . $thisFile);
        $lines = explode("\n", $filecontents);
        $results = array();

        if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;

        if (count($lines) > 0)
          {

          foreach ($lines as $line)
            {
            $confirmationFields = explode($fileSeparator, $line);

            /* Process this file line if it has enough fields. */

            if  (count($confirmationFields) >= 96)
              {
              $confirmationLine = array();

              $confirmationLine['fileName'] = $thisFile;
              $confirmationLine['status'] = $confirmationFields[0];
              $confirmationLine['cancelled'] = $confirmationFields[1];
              $confirmationLine['accountNumber'] = $confirmationFields[3];
              $confirmationLine['transactionID'] = intval($confirmationFields[5]);
              $confirmationLine['bankref'] = $confirmationFields[6];
              $confirmationLine['settlementDate'] = date('Y-m-d', datefmt_parse($Formatter_Date, $confirmationFields[8]));

              /* Parvest funds populate different fields in the confirmations file (Because they are not executed in the sme way?). Pick Nav Date accordingly. */

              if (strlen($confirmationFields[26]) > 0)
                {
                $confirmationLine['navDate'] = date('Y-m-d', datefmt_parse($Formatter_Date, $confirmationFields[26]));
                }
              else
                {
                $confirmationLine['navDate'] = date('Y-m-d', datefmt_parse($Formatter_Date, $confirmationFields[9]));
                }

              $confirmationLine['ISIN'] = $confirmationFields[10];
              $confirmationLine['category'] = $confirmationFields[14];
              if($confirmationLine['category'] != 'OPC')
              {
                     $confirmationLine['transactionID'] = substr($confirmationLine['transactionID'],1);
              }
              $confirmationLine['orderType'] = $confirmationFields[15];
              $confirmationLine['quantity'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $confirmationFields[17]));
              $confirmationLine['quantityMetric'] = $confirmationFields[18];

              /* Parvest funds populate different fields in the confirmations file (Because they are not executed in the sme way?). Pick Price accordingly. */

              if (strlen(str_replace(' ','', $confirmationFields[24])) > 0) // Use NAV if present, else use Deal Price (BNP Funds use Deal price, others use NAV.)
                {
                $confirmationLine['price'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $confirmationFields[24]));
                $confirmationLine['priceCurrency'] = $confirmationFields[25];
                }
              else
                {
                $confirmationLine['price'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $confirmationFields[19]));
                $confirmationLine['priceCurrency'] = $confirmationFields[20];
                }

              $confirmationLine['netCashSettlement'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $confirmationFields[74]));
              $confirmationLine['netCashSettlementCurrency'] = $confirmationFields[75];

              /* Parvest funds populate different fields in the confirmations file (Because they are not executed in the sme way?). Pick / Calculate Gross Cash accordingly. */

              if (strlen(str_replace(' ','', $confirmationFields[27])) > 0)
                {
                $confirmationLine['grossCashLocal'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $confirmationFields[27]));
                $confirmationLine['grossCashLocalCurrency'] = $confirmationFields[28];
                }
              else
                {
                $confirmationLine['grossCashLocal'] = $confirmationLine['quantity'] * $confirmationLine['price'];
                $confirmationLine['grossCashLocalCurrency'] = $confirmationFields[20];

                /* Make Gross cash negative if Net cash is negative. Easier than trying to figure out if the order is a buy or a sell. */

                if ($confirmationLine['netCashSettlement'] < 0)
                  {
                  $confirmationLine['grossCashLocal'] = $confirmationLine['grossCashLocal'] * (-1);
                  }
                }


              $confirmationLine['text'] = $line;

              /* Don't process confirmations for Sophis orders, only Venice orders.
                 Best judged by TransactionID > 3000000 (sophis operates around 1200000 ish.

              Condition removed - 3 Sep 2013, now that Sophis is de-Commissioned.

              if (intval($confirmationLine['transactionID']) > 3000000)
                {
                $results[] = $confirmationLine;
                }
              */
              // Ignore confirmation line for equities lines
              if($confirmationLine['category'] == 'OPC')
              {
                  $results[] = $confirmationLine;
              }


              if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . $confirmationLine['transactionID'] . ' : ' . $confirmationLine['orderType'] . ' ' . $confirmationLine['quantity'] . ' ' . $confirmationFields[15] . PHP_EOL;
              }

            } // $line

          } // If Count($lines)
        else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
          }

        // Save Confirmation file entries.
        $DoMoveFile = true;

        if (count($results) > 0)
          {
          if (set_tradeFilesConfirmationBNPParisRocheBrune_sqlsrv($results) === false)
            {
            $DoMoveFile = false;
            }
          }

        // Move Acknowledgement file to Processed directory.

        if ($DoMoveFile)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    moving RB file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

          try
            {
            $nowDate = New DateTime();
            }
          catch (Exception $e)
            {
            // In the case of an exception, probable because the timezone is not set in php.ini, default to Paris.
            date_default_timezone_set('Europe/Paris');
            $nowDate = New DateTime();
            }

          $newFile = $nowDate->format('YmdHis') . '_RB_' . $thisFile;

          $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
          if ($copyResult)
            {
            $capture = array();
            $capture['setID'] = 9;
            $capture['result'] = 'success';
            $capture['filename'] = $thisFile;
            $capture['priority'] = 0;
            $sql_DateEntered = get_DateNow_sqlsvr();
            $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

            unlink(($pendingDirectoryName . '/' . $thisFile));
            }
          else
            {
            	$capture = array();
            	$capture['setID'] = 9;
            	$capture['result'] = 'failed to copy';
            	$capture['filename'] = $thisFile;
            	$capture['priority'] = 0;
            	$sql_DateEntered = get_DateNow_sqlsvr();
            	$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
            }

          }
        else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
          }

        } // If preg_match()
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
        }

      } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false

  } catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  capturefailed('checkConfirmationsBNPParis, failed to process file : ' . $e->getMessage(), $thisFile);
  }












?>