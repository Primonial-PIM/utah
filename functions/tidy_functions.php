<?php
function tidy_page($html){
  
	$tidy = new Tidy();
  	$options = array('indent' => true);
 
  	$tidy->parseString($html, $options);
  	$tidy->cleanRepair();
 
  	return tidy_get_output($tidy);
}

function tidy_clip($html){

	$tidy = new Tidy();
	$options = array('show-body-only'=>true,'indent' => true);

	$tidy->parseString($html, $options);
	$tidy->cleanRepair();

	return tidy_get_output($tidy);
}

?>