<?php
// Assumes require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/logfile.php');

function bnp_getLoginScreen($ckfile){
	
	try{
		$Url="https://ssologin-bp2s.bnpparibas.com/SSOLoginAction.do";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive',
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5'
		));
	
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);

		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	} 
	catch (Exception $e) {
		$output="ERROR getLoginScreen ".$e->getMessage();
	}

  if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getLoginScreen\r");
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");

	return $output;
}

function bnp_postLoginScreen($ckfile,$post_params){

	try{
		$Url="https://ssologin-bp2s.bnpparibas.com/SSOLoginAction.do";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_REFERER, "https://ssologin-bp2s.bnpparibas.com");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_POST, substr_count( $post_params , "="));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive',
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5'
		));
		
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);

		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	}
	catch (Exception $e) {
		$output="ERROR postLoginScreen ".$e->getMessage();
	}
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "postLoginScreen\r");
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $post_params."\r");
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
	return $output;
}

function bnp_getPortalPage($ckfile){

	try{
		$Url="https://securities-client.bnpparibas.com/web/portal?lang=en";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_REFERER, "https://ssologin-bp2s.bnpparibas.com/SSOLoginAction.do");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive',
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5'
		));

		curl_setopt($ch, CURLINFO_HEADER_OUT, true);

		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	}
	catch (Exception $e) {
		$output="ERROR getPortalPage ".$e->getMessage();
	}
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getPortalPage\r");
	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
	return $output;
}
		
function bnp_getCustodyHoldings($ckfile){

	
	try{
		$Url="https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3GMpbeGjmufsMjtu/ep]4Gqbhfobnf]4EQUG%60QBHF%60MJTUF%60QPTJUJPOT]37nfov]4Eusvf]37bqqmz]4Eusvf&bcu]4EB3192&lang=en";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal/neolink/custody/safekeeping/allpositions/current.psml");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		//curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);
		
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive', 
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5',
				'Host: securities-link.bnpparibas.com'
		));
		
		
		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	}
	catch (Exception $e) {
		$output="ERROR getCustodyHoldings ".$e->getMessage();
	}
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getCustodyHoldings\r");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getCustodyHoldingsResponse\r");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $output."\n\n");
	return $output;
}		

function bnp_getCustodyHoldingsAgain($ckfile){

	try{
		$Url="https://securities-link.bnpparibas.com/web/LoadFilterList.do?pagename=PTF_PAGE_LISTE_POSITIONS&menu=true&apply=true";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive',
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5'
		));

		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	}
	catch (Exception $e) {
		$output="ERROR postCustodyHoldingsAgain ".$e->getMessage();
	}
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getCustodyHoldingsAgain\r");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
	return $output;
}

function bnp_postCustodyHoldingsAll($ckfile,$post_params){

	try{
		$Url="https://securities-link.bnpparibas.com/web/FilterBuilder.do?C_MODE=LAST";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal");
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_POST, substr_count( $post_params , "="));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Connection: keep-alive',
				'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language: en-us,en;q=0.5'
		));

		$output = curl_exec ($ch);
		$header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
	}
	catch (Exception $e) {
		$output="ERROR postCustodyHoldingsAll ".$e->getMessage();
	}
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getCustodyHoldingsAll\r");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $post_params."\r");
//	if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
	return $output;
}

function bnp_getConfirmations($ckfile){

    try{

        $Url="https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3GMpbeGjmufsMjtu/ep]4Gqbhfobnf]4EDBS%60QBHF%60MJTUF%60TFDVSJUJFT]37nfov]4Eusvf]37bqqmz]4Eusvf&bcu]4EB3192&lang=en";
        //$Url="https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3GMpbeGjmufsMjtu/ep]4Gqbhfobnf]4EDBS%60QBHF%60MJTUF%60TFDVSJUJFT]37nfov]4Eusvf]37bqqmz]4Eusvf&bcu]4EB3192&lang=en";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal/neolink/custody/settlement/confirmations/current.psml");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);

        curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5',
            'Host: securities-link.bnpparibas.com'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
        $output="ERROR getCustodyHoldings ".$e->getMessage();
    }
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getConfirmations\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getConfirmationsResponse\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $output."\n\n");
    return $output;
}

function bnp_getConfirmationsAgain($ckfile){

    try{
        $Url="https://securities-link.bnpparibas.com/web/LoadFilterList.do?pagename=CAR_PAGE_LISTE_SECURITIES&menu=true&apply=true";
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
        $output="ERROR postCustodyHoldingsAgain ".$e->getMessage();
    }
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "getConfirmationsAgain\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
    return $output;
}

function bnp_postConfirmationsAll($ckfile,$post_params){

    try{
        $Url="https://securities-link.bnpparibas.com/web/FilterBuilder.do?C_MODE=LAST";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_REFERER, "https://securities-link.bnpparibas.com/web/LoadFilterList.do?pagename=CAR_PAGE_LISTE_SECURITIES&menu=true&apply=true");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, substr_count( $post_params , "="));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
        $output="ERROR postConfirmationsAll ".$e->getMessage();
    }
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "postConfirmaitonsAll\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $post_params."\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");
    return $output;
}

function bnp_postGeneric($ckfile,$post_url, $post_params){

    try{
        $Url=$post_url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_REFERER, "https://securities-link.bnpparibas.com/web/LoadFilterList.do?pagename=CAR_PAGE_LISTE_SECURITIES&menu=true&apply=true");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, substr_count( $post_params , "="));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
    	if (NEOCAPTURE_DEBUG_ECHO) echo $e->getMessage();
        $output="ERROR postGeneric ".$e->getMessage();
    }

//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', "postConfirmaitonsAll\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $post_params."\r");
//    if (NEOCAPTURE_LOG_TO_FILE) logToFile(NEOCAPTURE_ROOT.'/tmp/log.txt', $header."\n\n");

    return $output;
}


function bnp_getGeneric($ckfile, $geturl){

    try{
        $Url=$geturl;
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_REFERER, "https://securities-client.bnpparibas.com/web/portal");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
        $output="ERROR getGeneric ".$e->getMessage();
    }
    return $output;
}

function getGeneric($geturl){

    try{
        $Url=$geturl;
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_REFERER, "www.example.com");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5'
        ));

        $output = curl_exec ($ch);
        $header=curl_getinfo($ch, CURLINFO_HEADER_OUT );
    }
    catch (Exception $e) {
        $output="ERROR getGeneric ".$e->getMessage();
    }
    return $output;
}
?>
