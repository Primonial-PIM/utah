<?php
/**
 * Created by PhpStorm.
 * User: rruzinda
 * Date: 3/8/14
 * Time: 10:23 AM
 */
require_once('localise/localise.php');
require_once('functions/db_functions.php');
require_once(LOGIN_PASSWORD_FILE);
define('DELIMITER', ';');

$sftp_server = 'hub.amf-france.org';
$port = '6321';
$sftp_user_name = 'ALTAROAM01';
$sftp_user_pass = 'shyaP9djj';
$directory = AMF_FILE_DIRECTORY;
$indexRDT = '1';


$timestamp = date('Y-m-dH:i:s');
//ini_set('display_errors', 1);
$sqlsvr_conn = renaissance_AR_connect();

//declare the SQL statement that will query the database
//$query = mssql_query($query_candidates); status 1 = Validated MO
$query = mssql_query('SELECT txn.TransactionParentID,txn.TransactionID, fund.FundAdministratorCode
FROM fn_tblTransactionSingle_SelectKD(0,null,null) AS txn,
fn_tblFund_SelectKD(null) AS fund
WHERE txn.TransactionTradeStatusID = 1 and txn.TransactionType in (4,5)
and txn.TransactionDecisionDate > "2014-02-26"
and txn.TransactionFund = fund.FundID
and txn.TransactionBroker > 0
and txn.TransactionSettlement > 0
and not exists ( SELECT tradeFile.TransactionParentID FROM tblTradeFileAmfReporting AS tradeFile
WHERE  tradeFile.TransactionParentID = txn.TransactionParentID)');


$localFilename = AMF_FILE_DIRECTORY  . '/' . $sftp_user_name . date('Ymd') . '.' . $indexRDT;
$remoteFilename = $sftp_user_name . date('Ymd') . '.' . $indexRDT;
$header  = 'E ' . $sftp_user_name . $timestamp . sprintf("%03d",$indexRDT);
$linesNbr = mssql_num_rows($query);
$trailer = 'F ' . $sftp_user_name . $timestamp . sprintf("%03d",$indexRDT) . sprintf("%08d",$linesNbr);


if (!mssql_num_rows($query)) {
    echo 'No candidates found' . PHP_EOL;
} else {

    $fp = fopen($localFilename, 'w');
    fputs($fp,$header . "\r\n");

    while ($row = mssql_fetch_array($query)) {
        //echo $row['TransactionParentID'] . " - " . $row['TransactionID']   . PHP_EOL;
        $execsArray = get_Trades_sqlsrv($row['TransactionParentID']);

        if (count($execsArray) > 0)
        {


            foreach ($execsArray as $val)
            {
                $reportTable = array();
                $line = implode (";", $val );

                //echo $row['TransactionParentID'] . " - " . $row['TransactionID']   . PHP_EOL;
                echo $line . PHP_EOL;
                $reportTable['TransactionParentID'] = $row['TransactionParentID'];
                $reportTable['TransactionID'] = $row['TransactionID'];
                $reportTable['FundAdministratorCode'] = $row['FundAdministratorCode'];
                $reportTable['TradeReportString'] = $line;
                echo "LONGUEUR LIGNE : " . strlen($line) . PHP_EOL;
                //$report[] = $reportTable;
                //print_r($reportTable);

                fputs($fp, $line . "\r\n" );
                // Entry in tblTradeFileAmfReporting table
                insertTradeFileAmf($reportTable);
                //fputs($fp, "\n");
                // Mark the entry in tblTradeFileExecutionReporting table as deleted
                // The entry has been inserted in the executions file
                set_tradeDone_sqlsrv($reportTable);


            }



        }


    }
    fputs($fp,$trailer . "\r\n");
    fclose($fp);

    // Send the file by SFTP

    $connection = ssh2_connect($sftp_server, $port);

    if (ssh2_auth_password($connection, $sftp_user_name, $sftp_user_pass)) {
        // initialize sftp
        $sftp = ssh2_sftp($connection);
        // Upload file
        echo "Connection successful, uploading file now...". PHP_EOL;
        $contents = file_get_contents($localFilename);

        file_put_contents("ssh2.sftp://$sftp/$remoteFilename", $contents);

        $copyResult = copy($localFilename, (AMF_FILE_DIRECTORY . '/sent/' . $remoteFilename));
        if ($copyResult)
        {
            unlink($localFilename);
        }


    } else {
        echo "Unable to authenticate with server". PHP_EOL;
    }

}

mssql_free_result($query);



function get_Trades_sqlsrv($recID){

    try
    {

        //ini_set('display_errors', 1);
        $sqlsvr_conn = renaissance_AR_connect();


        // Create a new stored prodecure
        $stmt = mssql_init('zzz_createAmfFile', $sqlsvr_conn);
        $knowledgeDate_Str = Null;
        mssql_bind($stmt, '@AuditID', &$recID, SQLINT4, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate_Str , SQLVARCHAR, false, false, 20);


        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}


function insertTradeFileAmf($results){


    try
    {

        $results_array = array();

        //print_r($results);

        $sqlsvr_conn = renaissance_AR_connect();


        $transactionID = $results['TransactionID'];
        $transactionParentID = $results['TransactionParentID'];
        $fundCode = $results['FundAdministratorCode'];
        $tradeReportString = $results['TradeReportString'];
        $knowledgeDate = Null;

        // Create a new stored prodecure
        $stmt = mssql_init('adp_tblTradeFileAmfReporting_InsertCommand', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {

            mssql_bind($stmt, '@TransactionID', &$transactionID, SQLINT4, false, false);
            mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);
            mssql_bind($stmt, '@FundCode', &$fundCode , SQLVARCHAR, false, false, 50);
            mssql_bind($stmt, '@TradeReportString', &$tradeReportString , SQLVARCHAR, false, false, 2000);
            mssql_bind($stmt, '@KnowledgeDate', &$$knowledgeDate ,SQLVARCHAR , false, false);

            mssql_execute($stmt);

            //if (mssql_num_rows($proc_result) > 0)
            //{
            //    $row = mssql_fetch_assoc($proc_result);
            //
            //    $results_array[$count++] = $row;
            //}

            // Free statement
            mssql_free_statement($stmt);
        }

     // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_tradeDone_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN', 'filename' and TransactionParentID
    // do whatever magic is required to change the status of execs in Venice

    try
    {

        $results_array = array();


        $sqlsvr_conn = renaissance_AR_connect();
        //ini_set('display_errors', 1);


            $UserID = 0;
            $Token  = 'Null';
            $transactionParentID = $results['TransactionParentID'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_tblTradeFileAmfReporting_Delete', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
                mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
                mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);

                mssql_execute($stmt);

                //if (mssql_num_rows($proc_result) > 0)
                //{
                //    $row = mssql_fetch_assoc($proc_result);
                //
                //    $results_array[$count++] = $row;
                //}

                // Free statement
                mssql_free_statement($stmt);
            }



    } catch (Exception $e)
    {
    }

    return $results_array;


}