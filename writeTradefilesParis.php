<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'WriteTradefiles, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');

// Generate Trade file name

$date = date('dmYHis');
$filename = PARIS_TRADEFILE_DIRECTORY . '/' . PARIS_TRADEFILE_NAME;

if (NEOCAPTURE_DEBUG_ECHO) echo 'Write Trade files, BNP Paris.' . PHP_EOL;

// skip if trade file already exists

if (file_exists($filename))
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  Trade file already exists. Abort and try later.' . PHP_EOL;
  } else
  {

  // Get trades from sqlsrv

  $SecondsSinceUpdate = PARIS_TRADEFILE_DELAY_SECONDS;

  $tradeArray = get_tradeStringsParis_sqlsrv($SecondsSinceUpdate);

  $fileContents = "";
  $results = array();
  $line = array();

  // Process pending trades (if there are any).

  if (count($tradeArray) > 0)
    {

    foreach ($tradeArray as $trade)
      {

      $fileContents .= $trade['tradeString'] . "\r\n";
      $line['RN'] = $trade['RN'];
      $line['filename'] = $filename;
      $results[] = $line;

      if (NEOCAPTURE_DEBUG_ECHO) echo '  Writing to file : ' . strval($trade['RN']) . ', `' . $trade['tradeString'] . '`' . PHP_EOL;

      }


    if (!(file_put_contents($filename, $fileContents) === false))
      {
      // trade sucess
      $results = set_tradeFilesDoneParis_sqlsrv($results);

      if (NEOCAPTURE_DEBUG_ECHO)
        {
        foreach ($results as $line)
          {
          echo '  Confirmed in DB : RN = ' . strval($line['RN']) . PHP_EOL;
          capturemessage('success');
          }
        }
      } else
      {
      // trade fail
      if (NEOCAPTURE_DEBUG_ECHO) echo 'Write to file FAILED.' . PHP_EOL;
      capturemessage('Write to file failed');

      }
    } else
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  No trades to save.' . PHP_EOL;
    capturemessage('No trades to save');
    }

  }

if (NEOCAPTURE_DEBUG_ECHO) echo '  Write Trade files, BNP Paris - END.' . PHP_EOL;

function capturemessage($message)
{
	$capture = array();
	$capture['setID'] = 12;
	$capture['dateandtime'] = convertToSQLDate(time());
	$capture['result'] = $message;

	$sql_DateEntered = get_DateNow_sqlsvr();
	add_capture_sqlserver($capture, $sql_DateEntered);
}

?>