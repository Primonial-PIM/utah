<?php
/**
 * Routines to capture and store daily prices from Morningstar
 * 
 * Query Venice Renaissance database for ISIN's and currency pairs which require a daily
 * price; submit ISIN and currency pairs to Morninstar https API; write results back to Venice 
 * Renaissance database
 * 
 * @package utah
 * 
 */
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);
/**
 * Application constants
 */
require_once('localise/localise.php');
/**
 * General functions for database connection and usage
 */
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
/**
 * Curl routines
 */
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
/**
 * Date conversion and other validation routines
 */
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
/**
 * Database routines for the capture tables
 */
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
/**
 * Database routines for collecting list of ISINs/currencies and writing results back
 */
require_once(NEOCAPTURE_ROOT . '/data/data_morningstar_prices.php');


/**
 * Live debut output
 * @param unknown_type $tag
 * @param unknown_type $message
 */
function LiveOutput($tag, $message)
  {
  $channel = "d994fTXc";
  file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
  }
/**
 * Get current prices from Morningstar API
 * @param array $msCodes Array of associative arrays of ISIN/Currency Pairs
 * @return array Array of associative arrays of results
 */
function getMorningstarPrices($msCodes)
  {
  try
    {

    $results = array();
    $resultline = array();
    $codecount = 0;
    $chunkcount = 0;
    $codestring = "";
    $currencystring = "";

    foreach ($msCodes AS $instrumentLine)
      { // $msCodes[1] as $msCurrency){
      $msCurrency = $instrumentLine['Currency'];

      if ($codecount < 500) // Arbitrary limit - Reflecting contractual limitations
        {

        //echo $msCurrency."<br>";
        //if ($codecount>0){ $currencystring.=",";}

        if (strlen(trim($instrumentLine['ISIN'])) > 0)
          {

          if ($chunkcount > 0)
            {
            $currencystring .= ",";
            $codestring .= ",";
            }

          $codestring .= trim($instrumentLine['ISIN']); // trim($msCodes[0][$codecount]);
          $currencystring .= "CU$$$$$" . trim($msCurrency);
          $chunkcount++;

          if ($chunkcount == 25 || $codecount == (count($msCodes) - 1))
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '  Getting Chunk from Morningstar.' . PHP_EOL;

            $url = "http://lt.morningstar.com/api/rest.svc/security_list/jrhe6v87w5/?pagesize=500&identifierList=" . $codestring . "&identifierCurrencyList=" . $currencystring;

            $xmlstring = getGeneric($url);

            $xml = simplexml_load_string($xmlstring);

            foreach ($xml->Security as $Security)
              {

              if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . $Security->attributes()->ISIN . " : " . $Security->attributes()->CurrencyId . " : " . $Security->LegalName . " : " . $Security->ClosePrice . PHP_EOL;

              $resultline['ISIN'] = strtoupper((string)$Security->attributes()->ISIN);
              $resultline['MorningstarID'] = (string)$Security->attributes()->SecurityId;
              $resultline['Currency'] = (string)$Security->attributes()->CurrencyId;
              $resultline['LegalName'] = (string)$Security->LegalName;
              $resultline['InitialPurchaseUnit'] = (string)$Security->InitialPurchaseUnit;
              $resultline['InitialPurchaseBase'] = (string)$Security->InitialPurchaseBase;
              $resultline['InitialPurchase'] = (string)$Security->InitialPurchase;
              $resultline['SubsequentInvestmentUnit'] = (string)$Security->SubsequentInvestmentUnit;
              $resultline['SubsequentInvestment'] = (string)$Security->SubsequentInvestment;
              $resultline['LegalStructure'] = (string)$Security->LegalStructure;
              $resultline['DomicileCountryName'] = (string)$Security->DomicileCountryName;
              $resultline['UCITS'] = (string)$Security->UCITS;
              $resultline['ClosePriceDate'] = (string)$Security->ClosePriceDate;
              $resultline['ClosePrice'] = (string)$Security->ClosePrice;
              $resultline['PriceCurrencyId'] = (string)$Security->PriceCurrencyId;
              $resultline['CutOffTime'] = (string)$Security->CutOffTime;
              $resultline['DealingType'] = (string)$Security->DealingType;
              $results[] = $resultline;
              }

            $chunkcount = 0;
            $codestring = "";
            $currencystring = "";

            } // if

          } // strlen(ISIN) > 0

        }
      $codecount++;
      }

    } catch (Exception $e)
    {
    //capturefailed('capture failed ' . $e->getMessage());
    return "failed";
    }
  return $results;
  }

// MAIN

if (NEOCAPTURE_DEBUG_ECHO) echo 'Morningstar Prices, Start' . PHP_EOL;

if (NEOCAPTURE_DEBUG_ECHO) echo '  Get Instrument IDs from Venice' . PHP_EOL;

$msCodes = get_morningstar_list_sqlsrv();

if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . count($msCodes[0]) . ' Instruments returned.' . PHP_EOL;

$results = GetMorningstarPrices($msCodes);

if (count($results) > 1)
  {
  $success = true;
  } else
  {
  $success = false;
  }


if ($success)
  {

  try
    {

    $capture = array();
    $capture['setID'] = 7;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $sql_DateEntered = get_DateNow_sqlsvr();

    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Capture ID : ' . $sqlsvr_id . PHP_EOL;

    $results_array = add_morningstar_prices_sqlsrv($msCodes, $results, $sql_DateEntered);

    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo 'write failed ' . $e->getMessage() . PHP_EOL;

    //capturefailed('write failed ' . $e->getMessage());
    exit;
    }

  }
else
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo 'capture failed';
  //capturefailed('failed');
  exit;
  }

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>