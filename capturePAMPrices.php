<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');

$public_dir = '/mnt/public';
$date = date('dmYHis');
$filename=$public_dir."/valeurs.csv";
$fileContents="";

//debug info
function LiveOutput($tag, $message)
{
	$channel = "d994fTXc";
	file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
}

function moveComma($performString){
	$posString="";
	//check if negative
	$posString=ereg_replace("-", "", $performString);
	if (strlen($posString)!=strlen($performString)){
		$negative=true;
	}else{
		$negative=false;
	}
	//find the comma
	$posComma=strpos($posString,",");
	if ($posComma==2){$posString="0,".ereg_replace(",", "", $posString);}
	if ($posComma==1){$posString="0,0".ereg_replace(",", "", $posString);}
	if ($negative){
		$result="-".$posString;
	} else {
		$result=$posString;
	}
	return $result;
}

function getPAMPrices(){

	global $filename, $fileContents;

	$funds="F0GBR04G59,F0GBR04G5B,F0GBR04G5D,F0GBR04QZJ,F000001UGS,F0000024R2,F00000J4FQ,F00000JV3B,F00000MO1X,F00000NOLI,F00000MJ7D,F00000MJ7E";

	$header="CISIN;VL;YTDPCT;YTDVOL;3ANSPCT;3ANSVOL;5ANSPCT;5ANSVOL;DATEVL";

	$urlbase="http://www.morningstar.fr/fr/funds/snapshot/p_snapshot.aspx?id=";

	$fundArray=explode(",",$funds);

	echo $header."<br>";
	$fileContents=$header."\r\n";

	foreach ($fundArray as $fund){

		$capture=array();
		$capture['ISIN']="";
		$capture['VL']="";
		$capture['YTDPct']="";
		$capture['YTDVol']="";
		$capture['3ansPct']="";
		$capture['3ansVol']="";
		$capture['5ansPct']="";
		$capture['5ansVol']="";
		$capture['DATEVL']="";

		$url=$urlbase.$fund;

		try{
			$url_content=(getGeneric($url));

			$doc = new DOMDocument();
			@$doc->loadHTML($url_content);

			$finder = new DomXPath($doc);

			//returnsTrailingDiv overviewQuickstatsDiv

			$tbody = $finder->query("//*[@id='overviewQuickstatsDiv']/table")->item(0);

			if (is_object($tbody)){
				if ($tbody->hasChildNodes()){

					foreach ($tbody->childNodes AS $row)
					{
						//echo "<br>";
						$rowcount++;
						$cellcount = 0;

						if ($rowcount > 0)
						{
							$holding = array();

							foreach ($row->childNodes AS $cell)
							{
								if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
								{

									$cellValue = $cell->nodeValue;

									if ($cellcount==0){
										if (!(strpos($cellValue,"VL")===false)){
											$valueRow=true;
											$capture['DATEVL']=substr($cellValue, -10);
											//echo "DATEVL : ".$capture['DATEVL']."<BR>";
										} else {
											$valueRow=false;
										}
											
										if (!(strpos($cellValue,"ISIN")===false)){
											$isinRow=true;
										} else {
											$isinRow=false;
										}
									}

				  			if ($cellcount==2){
				  				if($valueRow){
				  					$capture['VL']=ereg_replace("[^0-9,-]", "", $cellValue);
				  					//echo "VL : ".$capture['VL']."<BR>";
				  				}
				  				if($isinRow){
				  					$capture['ISIN']=$cellValue;
				  					//echo "ISIN : ".$capture['ISIN']."<BR>";
				  				}
				  				//echo $cellValue;
				  			}

				  			$cellcount++;
								}
							}
						}

					} // foreach ($tbody->childNodes AS $row)
				}
			}


			$tbody = $finder->query("//*[@id='returnsTrailingDiv']/table")->item(0);

			if(is_object($tbody)){
				if ($tbody->hasChildNodes()){

					foreach ($tbody->childNodes AS $row)
					{
						//echo "<br>";
						$rowcount++;
						$cellcount = 0;

						if ($rowcount > 0)
						{
							$holding = array();

							foreach ($row->childNodes AS $cell)
							{
								if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
								{

									$cellValue = $cell->nodeValue;

									if ($cellcount==0){
										if (!(strpos($cellValue,"but")===false)){
											$YTDRow=true;
										} else {
											$YTDRow=false;
										}
										if (!(strpos($cellValue,"3 an")===false)){
											$threeRow=true;
										} else {
											$threeRow=false;
										}
										if (!(strpos($cellValue,"5 an")===false)){
											$fiveRow=true;
										} else {
											$fiveRow=false;
										}
											
											
									}

				  			if ($cellcount==1){
				  				if($YTDRow){
				  					$capture['YTDPct']=ereg_replace("[^0-9,-]", "", $cellValue);
				  					if(strlen($capture['YTDPct'])<2){$capture['YTDPct']="";}
				  					if(strlen($capture['YTDPct'])>0){$capture['YTDPct']=moveComma($capture['YTDPct']);}
				  					//echo "YTDPct : ".$capture['YTDPct']."<BR>";
				  				}
				  				if($threeRow){
				  					$capture['3ansPct']=ereg_replace("[^0-9,-]", "", $cellValue);
				  					if(strlen($capture['3ansPct'])<2){$capture['3ansPct']="";}
				  					if(strlen($capture['3ansPct'])>0){$capture['3ansPct']=moveComma($capture['3ansPct']);}
				  					//echo "3ansPct : ".$capture['3ansPct']."<BR>";
				  				}
				  				if($fiveRow){
				  					$capture['5ansPct']=ereg_replace("[^0-9,-]", "", $cellValue);
				  					if(strlen($capture['5ansPct'])<2){$capture['5ansPct']="";}
				  					if(strlen($capture['5ansPct'])>0){$capture['5ansPct']=moveComma($capture['5ansPct']);}
				  					//echo "5ansPct : ".$capture['5ansPct']."<BR>";
				  				}

				  				//echo $cellValue;
				  			}

				  			$cellcount++;
								}
							}
						}

					} // foreach ($tbody->childNodes AS $row)
				}
			}

			$outline=$capture['ISIN'].";".$capture['VL'].";".$capture['YTDPct'].";".$capture['YTDVol'].";".
			$capture['3ansPct'].";".$capture['3ansVol'].";".$capture['5ansPct'].";".$capture['5ansVol'].";".$capture['DATEVL'];

			if (strlen($capture["ISIN"])>0){
				echo $outline."<br>";
				$fileContents.=$outline."\r\n";
			}
		}
		catch (Exception $e)
		{
			
			return "failed";
		}

	}

	if (!(file_put_contents($filename, $fileContents)===false)){
		echo $filename." sucess"."<br>";
		return "sucess";
	} else {
		echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
		return "write failed";
	}

}

//collect the post variables for the login


//first assume that session is still valid and try and go straight to the confirmations table

if (NEOCAPTURE_DEBUG_ECHO) echo 'PAM Prices, Start' . PHP_EOL;

$result = "failed";

//if first try is not successful then walk through logon procedure

if ($result == "failed")
{
	$complete = false;
	$trycount = 0;
	$loginCount = 0;
	do
	{
		switch ($result)
		{
			case "failed":
				if ($loginCount >= 6){
					$loginCount = 0;
					unlink($ckfile);
				}

				$result = getPAMPrices();
				$loginCount++;
				break;

			case "sucess":
				$complete = true;
				$success = true;
				break;
				
			case "write failed":
				$complete = true;
				$success = false;
				break;	
		}
		$trycount++;
		if ($trycount > 15)
		{
			$complete = true;
			$success = false;
		}

		if (NEOCAPTURE_DEBUG_ECHO) echo '    Next Result : '.$result . PHP_EOL;

	} while (!$complete);
}

if ($success)
{

	//extract data from xml


	//extract the table containing the holdings

	if (NEOCAPTURE_DEBUG_ECHO) echo 'Data Extracted : '. PHP_EOL;

	// write to database

	try
	{
		 

		 
	} catch (Exception $e)
	{
		capturefailed('write failed ' . $e->getMessage());
		exit;
	}

}
else
{
	
	exit;
}
/*
 @chmod ($ckfile, 0777);
 @chgrp ($ckfile, 'primonial');
 @chown ($ckfile, 'www-data');
 */
if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;



?>